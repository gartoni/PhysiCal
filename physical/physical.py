import numpy as np
import pandas as pd
import datetime as dt
import jinja2
import uuid

DATE_FORMAT = "%d/%m/%y"

weekdays = {'LUN': 0, 'MAR': 1, 'MER': 2, 'GIO': 3, 'VEN': 4}


class TeacherOverlapException(Exception):
    pass


def overlap(all_slots, slot, schedule):
    day1, h1, w1 = schedule.parse_slot(slot)

    for other_slot in all_slots:
        day2, h2, w2 = schedule.parse_slot(other_slot)

        if day1 == day2:
            if set(h1).intersection(h2) and set(w1).intersection(w2):
                return other_slot

    return None


def course_by_name(courses, name):
    for course in courses.values():
        if course.name == name:
            return course
    raise Exception("Course '%s' not found" % name)


def extrapolate_N_students(base_number, group, base_year, end_year):
    # constants found empirically by linear fitting
    def N_matricole_T(y):
        return 180 + 13.5 * (y - 2001)

    def N_matricole_M(y):
        return 100 + 7 * (y - 2001) + 20

    if group[0] == "T":
        extrapolated = base_number * N_matricole_T(end_year) / N_matricole_T(base_year)
    else:
        extrapolated = base_number * N_matricole_M(end_year) / N_matricole_M(base_year)

    return extrapolated


class Group:

    def __init__(self, gid, name, channels=None, student_fractions=None):
        self.id = gid
        self.name = name
        self.channels = channels
        self.student_fractions = student_fractions

    def __repr__(self):
        return self.name


class Course:

    def __init__(self, code, name, group, hours, teachers, tags, total_students):
        self.code = code
        self.name = name
        self.group = group
        self.hours = hours
        self.teachers = teachers
        self.tags = tags
        self.students = [total_students * f for f in group.student_fractions]
        # this list holds a list of (day, hours) pairs for each channel
        self.lecture_days = [[] for _ in range(len(self.teachers))]
        self.slots = [[] for _ in range(len(self.teachers))]
        self.pauses = [[] for _ in range(len(self.teachers))]

    def add_pause(self, pause, channel=None):
        if channel == None:
            for pc in self.pauses:
                pc.append(pause)
        else:
            self.pauses[channel].append(pause)

    def add_lecture_days_from_slot(self, channel, slot, classroom, schedule, comment=""):
        self.slots[channel].append((slot, classroom))

        day, hours, weeks = schedule.parse_slot(slot)
        N_hours = len(hours)

        for week in weeks:
            day_diff = day - schedule.weekday1[week].weekday()
            # this check may fail only for the first week (which doesn't always start on a Monday)
            if day_diff >= 0:
                new_date = schedule.weekday1[week] + dt.timedelta(day_diff)
                # check that we haven't gone past the semester's end
                if new_date <= schedule.semester_end:
                    if comment == "" or comment == "a":
                        self.lecture_days[channel].append((new_date, N_hours))

    def as_simulation_format(self):
        course_data = []
        N_channels = len(self.group.channels)

        for i, channel in enumerate(self.group.channels):
            channel_data = {
                "code": self.code,
                "name": self.name
            }

            if N_channels > 1:
                channel_data["code"] += "_%d" % i
                channel_data["name"] += " (%s)" % channel

            slots_data = []
            for slot in self.slots[i]:
                day, hours = slot[0].split(" ")[0:2]
                new_slot = {
                    "day": day,
                    "hours": hours,
                    "room": slot[1]
                }
                slots_data.append(new_slot)

            channel_data["slots"] = slots_data

            course_data.append(channel_data)

        return course_data

    def __repr__(self):
        return self.name


class Classroom:

    def __init__(self, name, seats, extended_name, weeks, building):
        self.name = name
        self.extended_name = extended_name
        self.building = building
        self.seats = seats
        self.cal = np.zeros((5, 12, weeks), dtype=str)
        self.slots = []
        self.courses = []
        self.comments = []
        self.links = []
        self.capacity_details = []

    def allocate(self, slot, course, chan, teacher, comment, schedule):
        overlapping_slot = overlap(self.slots, slot, schedule)
        if overlapping_slot is not None:
            raise Exception("Room %s is busy at %s" % (self.name, slot))

        self.slots.append(slot)
        self.courses.append([course.name, chan, teacher])
        self.comments.append(comment)

        channel = course.group.channels.index(chan)
        students = course.students[channel]

        self.capacity_details.append([slot, course.name, course.group.id, chan, teacher, students])

    def capacity_analysis(self, base_year, end_year):
        warnings = []
        errors = []

        for detail in self.capacity_details:
            N_students = extrapolate_N_students(detail[5], detail[2], base_year, end_year)
            new_detail = detail.copy()
            new_detail[2] = new_detail[2][0:2]  # keep only the first two characters of the group id (e.g. T2.A -> T2)
            new_detail[5] = N_students
            if N_students > self.seats:
                errors.append(new_detail)
            elif N_students > 0.9 * self.seats:
                warnings.append(new_detail)

        return warnings, errors

    def add_link(self, name, link):
        self.links.append('<a href="%s" target="blank">%s</a>' % (link, name))

    def streaming_link(self):
        if len(self.links) > 0:
            links = ", ".join(self.links)
            return "<h4>Link streaming: %s</h4>" % (links)
        return ""

    def to_html(self, schedule):
        matrix = np.full((12, 5), "", dtype=object)
        for slot, course, comment in zip(self.slots, self.courses, self.comments):
            day, hours, weeks = schedule.parse_slot(slot)
            for hour in hours:
                name, chan, teacher = course
                if matrix[hour - 8][day]:
                    matrix[hour - 8][day] += "<br><br>"
                groupclass = course_by_name(schedule.courses, name).group.id.split('.')[0]
                fcomment = comment
                if fcomment != '':
                    fcomment = "(" + comment + ")"
                if chan == '':
                    matrix[hour - 8][day] += '<span class="%s">%s %s</span><br> %s' % (
                        groupclass, name, fcomment, teacher)
                else:
                    matrix[hour - 8][day] += '<span class="%s">%s</span><br>(%s)<sub>%s</sub> %s' % (
                        groupclass, name, chan, comment, teacher)

                if len(weeks) < schedule.weeks:
                    matrix[hour - 8][day] += "<br><small>dal %s al %s</small>" % (
                        schedule.weekday1[weeks[0]].strftime(DATE_FORMAT),
                        schedule.weekday5[weeks[-1]].strftime(DATE_FORMAT))

        html = ""
        for i in range(12):
            html += f"<tr><td>{i + 8}-{i + 9}</td>"
            for j in range(5):
                html += f"<td>{matrix[i][j]}</td>"
            html += "</tr>\n"

        return html

    def __repr__(self):
        if np.isnan(self.seats):
            seats = "unknown"
        else:
            seats = str(self.seats)

        return "%s, capienza: %s" % (self.name, seats)


class Schedule:
    def __init__(self, xls_file, semester, semester_start, semester_end):
        self.datafile = xls_file
        self.semester = semester
        self.holidays = []
        self.holidays_as_strings = []

        # the next lines automatically initialises the weekday arrays by using the semester's start and end dates 
        self.semester_start = semester_start
        self.semester_end = semester_end
        self.weekday1 = []
        self.weekday5 = []
        current_day = semester_start
        while current_day <= semester_end:
            last_weekday = current_day + dt.timedelta(4 - current_day.weekday())

            self.weekday1.append(current_day)

            if last_weekday > semester_end:
                last_weekday = semester_end
            self.weekday5.append(last_weekday)

            current_day = last_weekday + dt.timedelta(3)

        self.weeks = len(self.weekday1)

        self.load_groups()
        self.load_classrooms()
        self.load_courses()

    def load_groups(self):
        self.groups = {}

        df = pd.read_excel(self.datafile, sheet_name="Groups")
        for data in df.values:
            if data[2] is not np.nan:
                self.groups[data[0]] = Group(data[0], name=data[1], channels=data[2].split(","),
                                             student_fractions=[float(x) for x in data[3].split(",")])
            else:
                self.groups[data[0]] = Group(data[0], name=data[1], channels=[""], student_fractions=[float(data[3]), ])

    def load_classrooms(self):
        self.classrooms = {}

        df = pd.read_excel(self.datafile, sheet_name="Classrooms")
        for data in df.values:
            if pd.isna(data[3]):
                seats = np.nan
            else:
                seats = int(data[3])
            self.classrooms[data[0]] = Classroom(
                name=data[0], seats=seats, extended_name=data[1],
                weeks=self.weeks, building=data[2]
            )
            if not pd.isna(data[4]):
                self.classrooms[data[0]].add_link("Meet", data[4])
            if not pd.isna(data[5]):
                self.classrooms[data[0]].add_link("Zoom", data[5])

    def load_courses(self):
        self.courses = {}
        all_teachers = []

        if self.semester == 1:
            df = pd.read_excel(self.datafile, sheet_name="Courses I sem")
        else:
            df = pd.read_excel(self.datafile, sheet_name="Courses II sem")

        for data in df.values:
            if data[3] is np.nan:
                tags = []
            else:
                tags = [t.strip() for t in data[3].split(",")]
            teachers_channels = [t.strip() for t in
                                 data[4].split(",")]  # commas separate teachers belonging to different "channels"
            try:
                total_students = float(data[6])
            except:
                total_students = None
            self.courses[data[0]] = Course(data[0], name=data[1], group=self.groups[data[2]],
                                           teachers=teachers_channels, hours=data[5], tags=tags,
                                           total_students=total_students)

            for teachers in teachers_channels:
                all_teachers += [t.strip() for t in teachers.split("/")]

        # remove duplicates
        all_teachers = list(set(all_teachers))
        self.teacher_slots = {t: [] for t in all_teachers}

    def parse_slot(self, slot):
        tokens = slot.split()
        if len(tokens) == 2:
            daystr, hourstr = tokens
            weeks = np.arange(self.weeks)
        elif len(tokens) == 3:
            daystr, hourstr, weekstr = tokens
            w1, w2 = weekstr.split('-')
            if w2 == "end":
                w2 = self.weeks
            weeks = np.arange(int(w1) - 1, int(w2))

        day = weekdays[daystr]
        h1, h2 = hourstr.split('-')
        hours = np.arange(int(h1), int(h2))

        return day, hours, weeks

    def _days_from_range(self, starts_on, ends_on=None):
        starts_on = dt.date.fromisoformat(starts_on)

        if ends_on is not None:
            ends_on = dt.date.fromisoformat(ends_on)
            if starts_on > ends_on:
                raise Exception("The ending date of a range (%s) must follow its starting date (%s)", ends_on,
                                starts_on)

        days = [starts_on, ]

        if ends_on is not None:
            while starts_on <= ends_on:
                days.append(starts_on)
                starts_on += dt.timedelta(1)

        return days

    def add_pause(self, course_name, starts_on, ends_on=None, channel=None):
        days = self._days_from_range(starts_on, ends_on)

        course = self.courses[course_name]
        for day in days:
            course.add_pause(day, channel)

    def add_pauses(self, courses, starts_on, ends_on=None):
        days = self._days_from_range(starts_on, ends_on)

        for day in days:
            for course in courses:
                course.add_pause(day)

    def add_holidays(self, starts_on, ends_on=None):
        days = self._days_from_range(starts_on, ends_on)
        new_holiday_string = days[0].strftime("%d/%m/%Y")
        if len(days) > 1:
            new_holiday_string += " - " + days[-1].strftime("%d/%m/%Y")

        for day in days:
            self.holidays.append(day)

        self.holidays_as_strings.append(new_holiday_string)

    def add_teacher_slot(self, teacher_string, slot, course='', room='', is_constraint=False, raise_exception=True):
        if teacher_string == "." or teacher_string.lower() == "da definire":
            return

        for teacher in [t.strip() for t in teacher_string.split("/")]:
            overlapping_slot = overlap([t_slot['slot'] for t_slot in self.teacher_slots[teacher]], slot, self)
            if overlapping_slot is not None and raise_exception:
                raise TeacherOverlapException(
                    "Teacher '%s' has an overlap between slots '%s' and '%s'" % (teacher, slot, overlapping_slot)
                )
            self.teacher_slots[teacher] += [{
                'slot': slot, 'course': course, 'room': room,
                'is_constraint': is_constraint
            }]

    def add_constraint(self, teacher, constraint, pad=False):
        for slot in constraint:
            if pad:
                room = 'Lezione'
            else:
                room = 'Indisponibile'
            self.add_teacher_slot(teacher_string=teacher, slot=slot, room=room, is_constraint=True)

    def add_lectures(self, name, slots, classrooms, channel=None, comment="", check_teacher_overlap=True):
        course = self.courses[name]

        if not channel:
            for i, (room, chan, teacher) in enumerate(zip(classrooms, course.group.channels, course.teachers)):
                room_obj = self.classrooms[room]
                for slot in slots:
                    room_obj.allocate(slot, course, chan, teacher, comment=comment, schedule=self)
                    course.add_lecture_days_from_slot(
                        channel=i, slot=slot, classroom=room_obj.extended_name,
                        schedule=self, comment=comment)
                    self.add_teacher_slot(
                        teacher_string=teacher, slot=slot, raise_exception=check_teacher_overlap,
                        course=course.name, room=room_obj.extended_name
                    )
        else:
            room_obj = self.classrooms[classrooms[0]]
            teacher = course.teachers[channel - 1]
            for slot in slots:
                room_obj.allocate(slot, course, course.group.channels[channel - 1], teacher, comment=comment,
                                  schedule=self)
                course.add_lecture_days_from_slot(
                    channel=channel - 1, slot=slot, classroom=room_obj.extended_name,
                    schedule=self, comment=comment
                )
                self.add_teacher_slot(
                    teacher_string=teacher, slot=slot, raise_exception=check_teacher_overlap,
                    course=course.name, room=room_obj.extended_name
                )

    def bygroup(self, grouplist, title=None):
        if len(grouplist) == 1:
            title = self.groups[grouplist[0]].name

        html = "<a name=%s><h3>%s</h3></a>\n" % (grouplist[0], title)
        html += '<table class="studgroup"><tr>'
        html += "<th>Insegnamento</th><th>Docente</th>"
        for day in weekdays.keys():
            html += "<th> %s </th>" % day
        html += "</tr>"

        courses = self.courses_by_group(grouplist)

        for c in courses:
            for chan in c.group.channels:
                if chan != '':
                    coursedescr = "%s (%s)" % (c.name, chan)
                else:
                    coursedescr = c.name

                html += "<tr>"
                html += "<td>" + coursedescr + "</td>"
                html += "<td>" + c.teachers[c.group.channels.index(chan)] + "</td>"
                row = [""] * 5
                for room in self.classrooms.values():
                    for slot, course, comment in zip(room.slots, room.courses, room.comments):
                        if course[0] == c.name and course[1] == chan:
                            day, hours, weeks = self.parse_slot(slot)
                            if row[day] != "":
                                row[day] += "<br>"
                            if comment == '':
                                row[day] += "%s <br>%d-%d" % (room.name, hours[0], hours[-1] + 1)
                            else:
                                row[day] += "%s (%s)<br>%d-%d" % (room.name, comment, hours[0], hours[-1] + 1)
                            if len(weeks) < self.weeks:
                                row[day] += "<br><small>dal %s<br>al %s</small>" % (
                                    self.weekday1[weeks[0]].strftime(DATE_FORMAT),
                                    self.weekday5[weeks[-1]].strftime(DATE_FORMAT))

                for i in range(5):
                    html += "<td>" + row[i] + "</td>"

            html += "</tr>"
        html += "</table>\n"
        return html

    def weekplan_bygroup(self, grouplist, tags=[]):
        matrix = np.full((12, 5), "", dtype=object)
        for room in self.classrooms.values():
            for slot, course in zip(room.slots, room.courses):
                day, hours, weeks = self.parse_slot(slot)
                # the third parameter is "teacher", which is not needed here
                name, chan, _ = course
                c = course_by_name(self.courses, name)
                if c.group.id not in grouplist:
                    continue
                if tags and not set(c.tags).intersection(tags):
                    continue
                for hour in hours:
                    if matrix[hour - 8][day]:
                        matrix[hour - 8][day] += "<br><br>*"
                    groupclass = course_by_name(self.courses, name).group.id.split('.')[0]
                    if chan == '':
                        matrix[hour - 8][day] += '<span class="%s">%s</span>' % (groupclass, name)
                    else:
                        matrix[hour - 8][day] += '<span class="%s">%s</span> (%s)' % (groupclass, name, chan)

                    if len(weeks) < self.weeks:
                        matrix[hour - 8][day] += "<br><small>dal %s al %s</small>" % (
                            self.weekday1[weeks[0]].strftime(DATE_FORMAT),
                            self.weekday5[weeks[-1]].strftime(DATE_FORMAT))

        html = "<table>"
        html += "<tr><th> ora </th>"
        for day in weekdays.keys():
            html += "<th> %s </th>" % day
        html += "</tr>\n"

        for i in range(12):
            html += "<tr><td>%d-%d</td>" % (i + 8, i + 9)
            for j in range(5):
                if '*' in matrix[i][j]:
                    html += '<td style="background-color: #ffff88">' + matrix[i][j] + "</td>"
                else:
                    html += "<td>" + matrix[i][j] + "</td>"
            html += "</tr>\n"
        html += "</table>\n\n"

        return html

    def weekplan_classrooms(self, free):
        matrix = np.full((12, 5), "", dtype=object)
        all_hours = list(range(8, 20))
        all_days = list(range(5))
        for hour in all_hours:
            for day in all_days:
                for room in self.classrooms.values():
                    if room.name not in [
                        'Amaldi', 'Cabibbo', 'Conversi', 'Majorana', 'Rasetti', 'Careri',
                        'Aula2', 'Aula3', 'Aula4', 'Aula5', 'Aula6', 'Aula7', 'Aula8'
                    ]:
                        continue
                    found_slot = False
                    for slot, course in zip(room.slots, room.courses):
                        s_day, s_hours, weeks = self.parse_slot(slot)
                        # the third parameter is "teacher", which is not needed here
                        name, chan, _ = course
                        if hour in s_hours and day == s_day:
                            found_slot = True
                            if not free:
                                if matrix[hour - 8][day]:
                                    matrix[hour - 8][day] += "<br><br>*"
                                matrix[hour - 8][day] += f'<span class="%s">{room.name} ({course[0]})</span>'
                    if free and not found_slot:
                        if matrix[hour - 8][day]:
                            matrix[hour - 8][day] += "<br><br>*"
                        matrix[hour - 8][day] += f'<span class="%s">{room.name}</span>'
                        if len(weeks) < self.weeks:
                            matrix[hour - 8][day] += "<br><small>dal %s al %s</small>" % (
                                self.weekday1[weeks[0]].strftime(DATE_FORMAT),
                                self.weekday5[weeks[-1]].strftime(DATE_FORMAT))

        html = "<table>"
        html += "<tr><th> ora </th>"
        for day in weekdays.keys():
            html += "<th> %s </th>" % day
        html += "</tr>\n"

        for i in range(12):
            html += "<tr><td>%d-%d</td>" % (i + 8, i + 9)
            for j in range(5):
                if '*' in matrix[i][j]:
                    html += '<td style="background-color: #ffff88">' + matrix[i][j] + "</td>"
                else:
                    html += "<td>" + matrix[i][j] + "</td>"
            html += "</tr>\n"
        html += "</table>\n\n"

        return html

    def courses_by_group(self, grouplist):
        courses = []
        for course in self.courses.values():
            if course.group.id in grouplist:
                courses.append(course)
        return courses

    def count_hours(self, course, chan_idx):
        hours = 0
        hours_with_pauses = 0
        for pair in course.lecture_days[chan_idx]:
            # we don't count holidays
            if pair[0] not in self.holidays:
                hours += pair[1]
                if pair[0] not in course.pauses[chan_idx]:
                    hours_with_pauses += pair[1]

        return hours, hours_with_pauses

    def course_hours(self):
        html = "<table>\n"
        html += "<tr>"
        html += "<th>Corso</th><th>Canale</th><th>Docente</th><th>Ore allocate</th><th>Ore effettive</th>"
        html += "</tr>"

        for course in self.courses.values():
            channels = course.group.channels
            for chan_idx, chan in enumerate(channels):
                teacher = course.teachers[channels.index(chan)]
                hours, hours_with_pauses = self.count_hours(course, chan_idx)

                color = "#FFF"
                if hours_with_pauses == 0:
                    color = "#d00000"
                elif hours_with_pauses < course.hours * 0.8:
                    color = "#e85d04"
                elif hours_with_pauses < course.hours * 1.049:
                    color = "#ffba08"
                elif hours_with_pauses < course.hours * 1.09:
                    color = "#fff6de"
                elif hours_with_pauses > course.hours * 1.5:
                    color = "#57cc99"
                elif hours_with_pauses > course.hours * 1.3:
                    color = "#80ed99"
                elif hours_with_pauses > course.hours * 1.15:
                    color = "#c7f9cc"

                html += "<tr style='background-color:%s'><td>%s</td> <td>%s</td> <td>%s</td>" % (
                    color, course.name, chan, teacher)
                html += "<td>%d/%d</td>" % (hours, course.hours)
                html += "<td>%d/%d</td></tr>\n" % (hours_with_pauses, course.hours)
        html += "</table>\n\n"
        return html

    def _base_key_dict(self):
        key_dict = {}
        if self.semester == 1:
            key_dict["semester"] = "I"
        else:
            key_dict["semester"] = "II"

        key_dict["AA"] = "%s/%s" % (self.semester_end.year - 1, self.semester_end.year)
        key_dict["semester_start"] = self.semester_start.strftime("%d/%m/%Y")
        key_dict["semester_end"] = self.semester_end.strftime("%d/%m/%Y")

        return key_dict

    def export_html(self, filename, masked_rooms=None, provisional=True, with_streaming_links=False):
        j_env = jinja2.Environment(loader=jinja2.FileSystemLoader("templates"))
        j_template = j_env.get_template("orario.html")

        key_dict = self._base_key_dict()

        groups = []
        groups.append(self.bygroup(['T1', 'T1.C'], title=self.groups['T1'].name))
        groups.append(self.bygroup(['T2', 'T2.A'], title=self.groups['T2'].name))
        groups.append(self.bygroup(['T3', 'T3.O', 'T3.A', 'T3.MQ'], title=self.groups['T3'].name))
        groups.append(self.bygroup(['M1']))

        if self.semester == 1:
            groups.append(self.bygroup(['M2']))

        key_dict["groups"] = groups
        key_dict["fermi_classrooms"] = []
        key_dict["marconi_classrooms"] = []
        key_dict["other_classrooms"] = []

        if provisional:
            key_dict["provisional"] = True

        rooms = {}
        for room in self.classrooms.values():
            # if the classroom is fake we skip it
            if room.extended_name == '-':
                continue
            if masked_rooms is not None:
                to_skip = False
                for masked_room in masked_rooms:
                    if room.extended_name == masked_room:
                        to_skip = True
                if to_skip:
                    continue
            # if the classroom is used then we print it
            if len(room.slots) > 0:
                if with_streaming_links:
                    link = room.streaming_link()
                else:
                    link = ""

                rooms[room.name] = {
                    "extended_name": room.extended_name,
                    "streaming_link": link,
                    "content": room.to_html(self)
                }

                lower_building = room.building.lower()
                if "fermi" in lower_building:
                    key_dict["fermi_classrooms"].append(room)
                elif "marconi" in lower_building:
                    key_dict["marconi_classrooms"].append(room)
                else:
                    key_dict["other_classrooms"].append(room)

        key_dict["rooms"] = rooms

        f = open(filename, "w")
        f.write(j_template.render(key_dict))
        f.close()

    def report(self, filename):
        j_env = jinja2.Environment(loader=jinja2.FileSystemLoader("templates"))
        j_template = j_env.get_template("report.html")

        key_dict = self._base_key_dict()
        key_dict["holidays"] = self.holidays_as_strings
        key_dict["course_hours"] = self.course_hours()

        weekplans = {
            "T1": self.weekplan_bygroup(['T1']),
            "T2, T2.A": self.weekplan_bygroup(['T2', 'T2.A']),
            "T3, T3.A, T3.O": self.weekplan_bygroup(['T3', 'T3.A', 'T3.O']),
            "M1, T,A,M,B,P,O": self.weekplan_bygroup(['M1'], tags=['T', 'M', 'B', 'O', 'A', 'P']),
            "M1, T,M,B": self.weekplan_bygroup(['M1'], tags=['T', 'M', 'B', 'O']),
            "M1, T,M,P": self.weekplan_bygroup(['M1'], tags=['T', 'M', 'P', 'O']),
            "M1, T,A,P": self.weekplan_bygroup(['M1'], tags=['T', 'A', 'P', 'O']),
            "M1, T,A": self.weekplan_bygroup(['M1'], tags=['T', 'A', 'O']),
            "M1, T,M": self.weekplan_bygroup(['M1'], tags=['T', 'M', 'O']),
            "M1, T,P": self.weekplan_bygroup(['M1'], tags=['T', 'P']),
            "M1, T": self.weekplan_bygroup(['M1'], tags=['T']),
            "M1, A": self.weekplan_bygroup(['M1'], tags=['A']),
            "M1, P": self.weekplan_bygroup(['M1'], tags=['P']),
            "M1, B": self.weekplan_bygroup(['M1'], tags=['B'])
        }
        if self.semester == 1:
            weekplans["M2, T,M,B,A,P,O"] = self.weekplan_bygroup(['M2'], tags=['T', 'M', 'B', 'O', 'A', 'P'])
            weekplans["M2, T,M,B"] = self.weekplan_bygroup(['M2'], tags=['T', 'M', 'B', 'O'])
            weekplans["M2, T,A,P"] = self.weekplan_bygroup(['M2'], tags=['T', 'A', 'P'])
            weekplans["M2, T"] = self.weekplan_bygroup(['M2'], tags=['T'])
            weekplans["M2, A"] = self.weekplan_bygroup(['M2'], tags=['A'])
            weekplans["M2, P"] = self.weekplan_bygroup(['M2'], tags=['P'])
            weekplans["M2, CM,T"] = self.weekplan_bygroup(['M2'], tags=['CM', 'T'])
            weekplans["M2, CM"] = self.weekplan_bygroup(['M2'], tags=['CM'])
            weekplans["M2, CCM"] = self.weekplan_bygroup(['M2'], tags=['CCM'])
            weekplans["M2, A,P"] = self.weekplan_bygroup(['M2'], tags=['A', 'P'])
            weekplans["M2, P,M"] = self.weekplan_bygroup(['M2'], tags=['M', 'P'])

        weekplans["Aule Libere"] = self.weekplan_classrooms(free=True)
        weekplans["Aule Occupate"] = self.weekplan_classrooms(free=False)

        key_dict["weekplans"] = weekplans

        f = open(filename, "w")
        f.write(j_template.render(key_dict))
        f.close()

    def simulation(self, filename, group):
        j_env = jinja2.Environment(loader=jinja2.FileSystemLoader("templates"))

        info_dict = {}
        if group == 'courses':
            j_template = j_env.get_template("simulazione_corsi.html")
            for group in self.groups.values():
                if group.id != "S":
                    info_dict[group.name] = []
                    for course in self.courses_by_group([group.id]):
                        info_dict[group.name] += course.as_simulation_format()
        elif group == 'teachers':
            j_template = j_env.get_template("simulazione_docenti.html")
            info_dict = {
                'Docenti': [{
                    'code': str(uuid.uuid4()), 'name': teacher,
                    'slots': [{
                        'day': slot_info['slot'].split(' ')[0],
                        'hours': slot_info['slot'].split(' ')[1],
                        'room': slot_info['room']
                    } for slot_info in slot_infos]
                } for teacher, slot_infos in sorted(self.teacher_slots.items()) if teacher != '.']
            }
        key_dict = self._base_key_dict()
        key_dict["item_lists"] = info_dict
        f = open(filename, "w")
        f.write(j_template.render(key_dict))
        f.close()

    def capacity_analysis(self, filename, years_forward=0):
        j_env = jinja2.Environment(loader=jinja2.FileSystemLoader("templates"))
        j_template = j_env.get_template("capacity.html")

        key_dict = self._base_key_dict()
        key_dict["AA_from"] = key_dict["AA"]
        key_dict["AA"] = "%s/%s" % (self.semester_end.year - 1 + years_forward, self.semester_end.year + years_forward)

        key_dict["fermi_classrooms"] = []
        key_dict["marconi_classrooms"] = []
        key_dict["other_classrooms"] = []

        base_year = self.semester_end.year - 1
        end_year = base_year + years_forward

        rooms = {}
        for room in self.classrooms.values():
            # if the classroom is used then we print it
            if len(room.slots) > 0:
                capacity_warnings, capacity_errors = room.capacity_analysis(base_year, end_year)
                has_capacity_issues = len(capacity_warnings) > 0 or len(capacity_errors) > 0

                rooms[room.name] = {
                    "name": room.name,
                    "building": room.building,
                    "extended_name": room.extended_name,
                    "seats": room.seats,
                    "capacity_warnings": capacity_warnings,
                    "capacity_errors": capacity_errors,
                    "has_capacity_issues": has_capacity_issues
                }

                lower_building = room.building.lower()
                if "fermi" in lower_building:
                    key_dict["fermi_classrooms"].append(rooms[room.name])
                elif "marconi" in lower_building:
                    key_dict["marconi_classrooms"].append(rooms[room.name])
                else:
                    key_dict["other_classrooms"].append(rooms[room.name])

        key_dict["rooms"] = rooms

        f = open(filename, "w")
        f.write(j_template.render(key_dict))
        f.close()
