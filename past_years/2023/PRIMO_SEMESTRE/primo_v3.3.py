'''
Created on Jun 16, 2021

@author: lorenzo
'''

import datetime as dt
from physical import physical

schedule = physical.Schedule("lectures_data.xlsx", semester=1, semester_start=dt.date(2022, 9, 22), semester_end=dt.date(2023, 1, 13))

# holidays
schedule.add_holidays("2022-10-31", "2022-11-01")
schedule.add_holidays("2022-12-07", "2022-12-08")
schedule.add_holidays("2022-12-23", "2023-01-07")

# T1
schedule.add_lectures("analisi", ["LUN 10-12", "MAR 12-14", "MER 10-12", "GIO 16-17"], ["Aula3"], channel=1)
schedule.add_lectures("analisi", ["LUN 16-18", "MAR 8-10", "MER 12-14", "GIO 10-11"], ["Aula3"], channel=2)

schedule.add_lectures("analisi", ["LUN 10-12", "MAR 12-14", "MER 10-12", "GIO 16-17"], ["Aula4"], channel=3)
schedule.add_lectures("analisi", ["LUN 14-16", "MAR 8-10", "MER 12-13", "GIO 10-12"], ["Aula4"], channel=4)

schedule.add_lectures("geometria", ["MAR 14-16", "MER 8-10", "GIO 17-18", "VEN 8-10"], ["Aula3"], channel=1)
schedule.add_lectures("geometria", ["MAR 10-12", "MER 14-16", "GIO 11-12", "VEN 11-13"], ["Aula3"], channel=2)

schedule.add_lectures("geometria", ["LUN 8-10", "MER 8-10", "GIO 17-18", "VEN 8-10"], ["Aula4"], channel=3)
schedule.add_lectures("geometria", ["MAR 10-12", "MER 13-14", "GIO 12-14", "VEN 11-13"], ["Aula4"], channel=4)

schedule.add_lectures("labcal", ["GIO 14-16", "VEN 10-11"], ["Aula3"], channel=1)
schedule.add_lectures("labcal", ["GIO 8-10", "VEN 13-14"], ["Aula3"], channel=2)

schedule.add_lectures("labcal", ["GIO 14-16", "VEN 10-11"], ["Aula4"], channel=3)
schedule.add_lectures("labcal", ["MER 14-16", "VEN 13-14"], ["Aula4"], channel=4)

# esercitazioni labcalc
schedule.add_lectures("labcal", ["MER 14-17"], ["LabCalc"], channel=1, comment="a")
schedule.add_lectures("labcal", ["GIO 9-12"], ["Aula17"], channel=1, comment="b")

schedule.add_lectures("labcal", ["LUN 13-16"], ["Aula17"], channel=2, comment="a")
schedule.add_lectures("labcal", ["GIO 13-16"], ["Aula17"], channel=2, comment="b")

schedule.add_lectures("labcal", ["MAR 9-12"], ["LabCalc"], channel=3, comment="b")
schedule.add_lectures("labcal", ["VEN 15-18"], ["LabCalc"], channel=3, comment="a")

schedule.add_lectures("labcal", ["LUN 16-19"], ["LabCalc"], channel=4, comment="a")
schedule.add_lectures("labcal", ["VEN 15-18"], ["Aula17"], channel=4, comment="b")

# T2
schedule.add_lectures("vector", ["MER 15-18", "GIO 8-10", "VEN 16-18"], ["Amaldi"], channel=1)
schedule.add_lectures("vector", ["MER 15-18", "GIO 8-10", "VEN 16-18"], ["Aula6"], channel=2)
schedule.add_lectures("vector", ["MER 15-18", "GIO 8-10", "VEN 16-18"], ["Aula7"], channel=3)

schedule.add_lectures("mecraz", ["LUN 14-15", "MAR 10-12", "VEN 14-16"], ["Amaldi"], channel=1)
schedule.add_lectures("mecraz", ["LUN 14-15", "MAR 10-12", "VEN 14-16"], ["Aula6"], channel=2)
schedule.add_lectures("mecraz", ["LUN 14-15", "MAR 10-12", "VEN 14-16"], ["Aula7"], channel=3)

schedule.add_lectures("labfiscomp", ["LUN 15-16", "MAR 8-10"], ["Amaldi"], channel=1)
schedule.add_lectures("labfiscomp", ["LUN 15-16", "MAR 8-10"], ["Aula6"], channel=2)
schedule.add_lectures("labfiscomp", ["LUN 15-16", "MAR 8-10"], ["Aula7"], channel=3)

# esercitazioni lab fis comp
schedule.add_lectures("labfiscomp", ["LUN 10-13"], ["Aula17"], channel=1, comment="a")
schedule.add_lectures("labfiscomp", ["GIO 16-19"], ["Aula17"], channel=1, comment="b")

schedule.add_lectures("labfiscomp", ["MAR 16-19"], ["LabCalc"], channel=2, comment="a")
schedule.add_lectures("labfiscomp", ["GIO 16-19"], ["LabCalc"], channel=2, comment="b")

schedule.add_lectures("labfiscomp", ["MAR 13-16"], ["LabCalc"], channel=3, comment="a")
schedule.add_lectures("labfiscomp", ["MER 10-13"], ["LabCalc"], channel=3, comment="b")

schedule.add_lectures("termo", ["LUN 16-18", "GIO 10-12"], ["Amaldi"], channel=1)
schedule.add_lectures("termo", ["LUN 16-18", "GIO 10-12"], ["Aula6"], channel=2)
schedule.add_lectures("termo", ["LUN 16-18", "GIO 10-12"], ["Aula7"], channel=3)

schedule.add_lectures("termo", ["MAR 14-18"], ["LabTermo"], channel=1)
schedule.add_lectures("termo", ["MER 10-14"], ["LabTermo"], channel=2)
schedule.add_lectures("termo", ["GIO 14-18"], ["LabTermo"], channel=3)

# T2.A
# Melchiorri chiede di non fare lezione lunedì e venerdì
schedule.add_lectures("astronomia", ["MAR 12-13", "MER 8-10", "GIO 12-14"], ["Conversi"])

# T3
schedule.add_lectures("segnali", ["LUN 8-10", "MER 10-12"], ["Amaldi"], channel=1)
schedule.add_lectures("segnali", ["LUN 8-10", "MER 10-12"], ["Aula6"], channel=2)
schedule.add_lectures("segnali", ["LUN 8-10", "MER 10-12"], ["Aula7"], channel=3)

schedule.add_lectures("segnali", ["MAR 9-13"], ["LabSS-LabAstro"], channel=1)
schedule.add_lectures("segnali", ["MER 15-19"], ["LabSS-LabAstro"], channel=2)
schedule.add_lectures("segnali", ["LUN 15-19"], ["LabSS-LabAstro"], channel=3)

schedule.add_lectures("quantum", ["LUN 11-12", "MAR 16-18", "MER 12-14", "VEN 12-14"],["Amaldi"], channel=1)
schedule.add_lectures("quantum", ["LUN 11-12", "MAR 16-18", "MER 12-14", "VEN 12-14"],["Aula6"], channel=2)
schedule.add_lectures("quantum", ["LUN 11-12", "MAR 16-18", "MER 12-14", "VEN 12-14"],["Aula7"], channel=3)

schedule.add_lectures("statmech", ["LUN 10-11", "MAR 14-16", "VEN 10-12"], ["Amaldi"], channel=1)
schedule.add_lectures("statmech", ["LUN 10-11", "MAR 14-16", "VEN 10-12"], ["Aula6"], channel=2)
schedule.add_lectures("statmech", ["LUN 10-11", "MAR 14-16", "VEN 10-12"], ["Aula7"], channel=3)

# T3.A
schedule.add_lectures("quantstat", ["LUN 10-13", "MAR 16-18", "MER 12-14", "VEN 12-14"], ["Careri"])

schedule.add_lectures("astro", ["LUN 13-15"], ["Aula3"])
schedule.add_lectures("astro", ["GIO 8-10"], ["Aula4"])
schedule.add_lectures("astro", ["VEN 14-15"], ["Aula3"])

schedule.add_lectures("astrofluo", ["MAR 12-14", "MER 8-10", "GIO 14-15"], ["Aula7"])

schedule.add_lectures("labastro", ["MAR 10-12", "GIO 10-12"], ["Conversi"])
schedule.add_lectures("labastro", ["GIO 15-19", "VEN 15-19"], ["LabSS-LabAstro"], comment="all")

# T3.O
schedule.add_lectures("prob", ["MER 14-15", "GIO 12-14", "VEN 8-10"], ["Amaldi"])

# M1
schedule.add_lectures("rqm.1", ["MAR 14-16"], ["Aula4"])
schedule.add_lectures("rqm.1", ["GIO 14-16", "VEN 8-9"], ["Aula6"])

schedule.add_lectures("rqm.2", ["MAR 14-16", "GIO 14-16", "VEN 8-9"], ["Conversi"])

schedule.add_lectures("condmat.1", ["MAR 16-18", "MER 16-17", "VEN 16-18"], ["Aula3"])
schedule.add_lectures("condmat.2", ["MAR 16-18", "MER 16-17", "VEN 16-18"], ["Aula4"])

schedule.add_lectures("critical", ["LUN 11-12"], ["Rasetti"])
schedule.add_lectures("critical", ["MAR 12-14", "GIO 16-18"], ["Careri"], check_teacher_overlap=False)

schedule.add_lectures("generale", ["LUN 12-14", "MER 17-18", "VEN 14-16"], ["Aula4"])

schedule.add_lectures("bio", ["LUN 8-9", "MAR 14-16", "GIO 8-10"], ["Rasetti"])

schedule.add_lectures("physlab.1", ["LUN 16-18", "MAR 10-12"], ["Rasetti"])

schedule.add_lectures("physlab.2", ["MER 14-16", "VEN 10-12"], ["Rasetti"])

schedule.add_lectures("physlab.3", ["LUN 16-18", "MAR 10-12"], ["Careri"])

schedule.add_lectures("compmet.1", ["LUN 8-11"], ["LabCalc"])
schedule.add_lectures("compmet.1", ["MAR 8-10", "GIO 12-14"], ["Majorana"])

schedule.add_lectures("compmet.2", ["LUN 12-16"], ["LabCalc"])
schedule.add_lectures("compmet.2", ["VEN 9-11"], ["LabCalc"])

schedule.add_lectures("compmet.3", ["MER 8-10", "GIO 12-14", "VEN 9-10"], ["Aula6"])

schedule.add_lectures("compmet.4", ["LUN 14-16"], ["Majorana"])
schedule.add_lectures("compmet.4", ["MER 14-16"], ["Majorana"], check_teacher_overlap=False)
schedule.add_lectures("compmet.4", ["VEN 11-14"], ["LabCalc"])

schedule.add_lectures("softmatter", ["LUN 12-13"], ["Majorana"])
schedule.add_lectures("softmatter", ["MER 12-14", "GIO 12-14"], ["Rasetti"])

schedule.add_lectures("nlquantumoptics", ["MER 10-12", "GIO 10-12", "VEN 12-13"], ["Rasetti"])

schedule.add_lectures("superiore", ["MER 10-12", "GIO 8-10 1-5", "VEN 10-12"], ["Careri"])

schedule.add_lectures("processi", ["MER 12-14", "GIO 10-12", "VEN 13-14"], ["Majorana"])

schedule.add_lectures("astrolab", ["MAR 16-18", "GIO 16-18"], ["Rasetti"])
schedule.add_lectures("astrolab", ["VEN 12-13"], ["Majorana"])

schedule.add_lectures("biochem", ["MAR 8-10"], ["Rasetti"])
schedule.add_lectures("biochem", ["GIO 15-16", "VEN 14-16"], ["Aula5"])

# qui serve un'aula da almeno ~70 posti
schedule.add_lectures("group", ["LUN 14-15", "GIO 8-10"], ["Conversi"])
schedule.add_lectures("group", ["MER 10-12"], ["Majorana"])

# M2
schedule.add_lectures("complex", ["VEN 16-19"], ["Careri"])
schedule.add_lectures("complex", ["GIO 16-18"], ["Aula6"])

schedule.add_lectures("quantinfo", ["MAR 8-10", "GIO 12-14"], ["Careri"])
schedule.add_lectures("quantinfo", ["MER 13-14"], ["Aula5"])

schedule.add_lectures("stoca", ["LUN 14-16", "MER 16-18", "VEN 15-16"], ["Careri"])

schedule.add_lectures("quantumfield", ["MER 11-13", "VEN 12-13"], ["Conversi"])
schedule.add_lectures("quantumfield", ["GIO 14-16"], ["Rasetti"])

schedule.add_lectures("disordered", ["LUN 10-12", "VEN 10-12"], ["Conversi"], check_teacher_overlap=False)
schedule.add_lectures("disordered", ["MER 14-15"], ["Careri"])

schedule.add_lectures("manybody", ["MAR 10-12", "MER 8-9", "GIO 8-10"], ["Majorana"])

schedule.add_lectures("surface", ["MAR 16-18", "MER 9-10", "GIO 10-12"], ["Aula5"])

schedule.add_lectures("weak", ["VEN 14-15"], ["Conversi"])
schedule.add_lectures("weak", ["LUN 14-16", "MER 16-18"], ["Rasetti"])

schedule.add_lectures("expgrav", ["LUN 10-12", "MER 10-11", "GIO 12-14"], ["Aula5"])

schedule.add_lectures("intrograv", ["MAR 8-10", "VEN 8-10"], ["Aula5"])
schedule.add_lectures("intrograv", ["MER 14-15"], ["Aula6"])

schedule.add_lectures("labcalcadv", ["GIO 8-10", "VEN 10-12"], ["Aula5"])
schedule.add_lectures("labcalcadv", ["MAR 10-12"], ["LabMAT"])

schedule.add_lectures("spaceastro", ["LUN 12-14", "MAR 14-16", "GIO 14-15"], ["Aula5"])

schedule.add_lectures("astropart", ["LUN 8-10", "MAR 12-13", "VEN 12-14"], ["Aula5"])

schedule.add_lectures("evoluzione", ["MER 16-18", "VEN 14-16"], ["Majorana"])

schedule.add_lectures("neutrinos", ["LUN 12-14"], ["Rasetti"])
schedule.add_lectures("neutrinos", ["GIO 10-12"], ["Careri"])
schedule.add_lectures("neutrinos", ["GIO 9-10 6-end"], ["Careri"])

schedule.add_lectures("medical", ["MER 14-16", "GIO 16-18", "VEN 16-18"], ["Aula5"])

schedule.add_lectures("autograv", ["MAR 16-18", "GIO 16-18"], ["Majorana"])

schedule.add_lectures("cosmo", ["LUN 16-18", "VEN 16-18"], ["Majorana"])

schedule.add_lectures("collider", ["LUN 16-18"], ["Aula5"])
schedule.add_lectures("collider", ["MAR 14-16", "MER 9-10"], ["Majorana"])

schedule.add_lectures("solidsensors", ["LUN 10-12", "MAR 12-14", "VEN 11-12"], ["Majorana"])

schedule.add_lectures("solids", ["MAR 12-14", "VEN 13-16"], ["Rasetti"])

schedule.add_lectures("stat_ml", ["LUN 16-18"], ["Aula4"])
schedule.add_lectures("stat_ml", ["MAR 14-16", "MER 15-16"], ["Careri"])

# altri CAD
schedule.add_lectures("ScienzeAmbientali", ["MAR 12-14"], ["Aula6"])
schedule.add_lectures("ScienzeAmbientali", ["MER 14-16"], ["Conversi"])
schedule.add_lectures("ScienzeAmbientali", ["GIO 14-16"], ["Majorana"])

# impegni non didattici
schedule.add_lectures("seminari", ["LUN 16-18", "MAR 16-18", "MER 16-18", "GIO 16-18", "VEN 16-18"], ["Conversi"])
schedule.add_lectures("studenti", ["LUN 8-19", "MAR 8-19", "MER 8-19", "GIO 8-19", "VEN 8-19"], ["Aula8"])
schedule.add_lectures("CdD/CAD", ["GIO 14-19"], ["Amaldi"])
schedule.add_lectures("dottorato", ["LUN 8-19", "MAR 8-19", "MER 8-19", "GIO 8-19", "VEN 8-19"], ["Aula2"])

# questi servono per i passaggi di anno
schedule.add_lectures("dottorato", ["GIO 15-19"], ["Aula7"])
schedule.add_lectures("dottorato", ["LUN 15-16 3-4"], ["Conversi"])
schedule.add_lectures("dottorato", ["VEN 15-16 3-4"], ["Conversi"])

schedule.export_html("2023/PRIMO_SEMESTRE/v3.3_orario.html", provisional=True)
schedule.report("2023/PRIMO_SEMESTRE/v3.3_report.html")
schedule.simulation("2023/PRIMO_SEMESTRE/v3.3_simulazione.html")
