'''
Created on Jun 16, 2021

@author: lorenzo
'''

import datetime as dt
from physical import physical

schedule = physical.Schedule("lectures_data.xlsx", semester=1, semester_start=dt.date(2022, 9, 22), semester_end=dt.date(2023, 1, 13))

# holidays
schedule.add_holidays("2022-11-01")
schedule.add_holidays("2022-12-08")
schedule.add_holidays("2022-12-23", "2023-01-07")

# T1
schedule.add_lectures("analisi", ["MAR 8-10", "MER 11-13", "GIO 10-12", "VEN 11-12"], ["Aula3"], channel=1)
schedule.add_lectures("analisi", ["MAR 11-12", "MER 10-12", "GIO 9-11", "VEN 10-12"], ["Aula4"], channel=2)
schedule.add_lectures("analisi", ["MAR 10-12", "MER 11-12", "GIO 8-10", "VEN 10-12"], ["Aula6"], channel=3)
schedule.add_lectures("analisi", ["MAR 10-12", "MER 11-12", "GIO 8-10", "VEN 10-12"], ["Aula7"], channel=4)

schedule.add_lectures("geometria", ["MAR 10-12", "MER 9-11", "GIO 12-13", "VEN 9-11"], ["Aula3"], channel=1)
schedule.add_lectures("geometria", ["MAR 8-9", "MER 8-10", "GIO 11-13", "VEN 8-10"], ["Aula4"], channel=2)
schedule.add_lectures("geometria", ["MAR 8-10", "MER 8-10", "GIO 12-13", "VEN 8-10"], ["Aula6"], channel=3)
schedule.add_lectures("geometria", ["MAR 8-10", "MER 8-10", "GIO 12-13", "VEN 8-10"], ["Aula7"], channel=4)

schedule.add_lectures("labcal", ["MER 8-9", "GIO 8-10"], ["Aula3"], channel=1)
schedule.add_lectures("labcal", ["MAR 9-11", "GIO 8-9"], ["Aula4"], channel=2)
schedule.add_lectures("labcal", ["MER 10-11", "GIO 10-12"], ["Aula6"], channel=3)
schedule.add_lectures("labcal", ["MER 10-11", "GIO 10-12"], ["Aula7"], channel=4)
# esercitazioni labcalc
schedule.add_lectures("labcal", ["LUN 16-19"], ["LabCalc"], channel=1, comment="b")
schedule.add_lectures("labcal", ["MER 16-19"], ["LabCalc"], channel=1, comment="a")

schedule.add_lectures("labcal", ["LUN 13-16"], ["Aula17"], channel=2, comment="a")
schedule.add_lectures("labcal", ["GIO 14-17"], ["LabCalc"], channel=2, comment="b")

schedule.add_lectures("labcal", ["LUN 10-13"], ["Aula17"], channel=3, comment="b")
schedule.add_lectures("labcal", ["MAR 15-18"], ["LabCalc"], channel=3, comment="a")

schedule.add_lectures("labcal", ["MER 13-16"], ["LabCalc"], channel=4, comment="b")
schedule.add_lectures("labcal", ["GIO 14-17"], ["Aula17"], channel=4, comment="a")

# T2
schedule.add_lectures("vector", ["LUN 8-10", "MAR 15-16", "MER 14-16", "VEN 10-12"], ["Amaldi"], channel=1)
schedule.add_lectures("vector", ["LUN 11-13", "MER 10-12", "VEN 9-12"], ["Majorana"], channel=2)
schedule.add_lectures("vector", ["LUN 9-11", "MER 14-16", "GIO 13-14", "VEN 12-14"], ["Aula7"], channel=3)

# Gualtieri chiede di avere il lunedì o il venerdì libero
schedule.add_lectures("mecraz", ["MAR 12-14", "MER 13-14"], ["Amaldi"], channel=1)
schedule.add_lectures("mecraz", ["VEN 12-14"], ["Amaldi"], channel=1)

schedule.add_lectures("mecraz", ["MAR 12-14", "MER 12-13"], ["Majorana"], channel=2)
schedule.add_lectures("mecraz", ["VEN 12-14"], ["Majorana"], channel=2)

schedule.add_lectures("mecraz", ["MAR 12-14", "MER 13-14"], ["Aula7"], channel=3)
schedule.add_lectures("mecraz", ["GIO 14-16"], ["Aula7"], channel=3)

# il primo canale va di martedì perché lunedì altrimenti avrebbe 10 ore filate di lezione+laboratorio
schedule.add_lectures("labfiscomp", ["MAR 10-12", "GIO 13-14"], ["Amaldi"], channel=1)
schedule.add_lectures("labfiscomp", ["LUN 9-11", "MAR 11-12"], ["Majorana"], channel=2)
schedule.add_lectures("labfiscomp", ["LUN 14-16", "MER 12-13"], ["Aula7"], channel=3)
# esercitazioni lab fis comp
schedule.add_lectures("labfiscomp", ["VEN 16-19"], ["LabCalc"])

schedule.add_lectures("termo", ["LUN 10-12", "GIO 10-12"], ["Amaldi"], channel=1)

schedule.add_lectures("termo", ["LUN 14-16", "GIO 16-18"], ["Aula6"], channel=2)

schedule.add_lectures("termo", ["LUN 11-13", "GIO 16-18"], ["Aula7"], channel=3)

schedule.add_lectures("termo", ["LUN 14-18"], ["LabTermo"], channel=1)
schedule.add_lectures("termo", ["MER 14-18"], ["LabTermo"], channel=2)
schedule.add_lectures("termo", ["MAR 15-19"], ["LabTermo"], channel=3)

# T2.A
schedule.add_lectures("astronomia", ["MAR 8-10", "MER 8-10", "GIO 8-9"], ["Conversi"])

# T3
# Vignati è impegnato mercoledì pomeriggio quindi il suo canale ha lezione di venerdì
schedule.add_lectures("segnali", ["LUN 10-12"], ["Aula3"], channel=1)
schedule.add_lectures("segnali", ["VEN 15-17"], ["Aula6"], channel=1)

schedule.add_lectures("segnali", ["LUN 10-12", "MER 12-14"], ["Aula4"], channel=2)
schedule.add_lectures("segnali", ["LUN 10-12", "MER 12-14"], ["Aula6"], channel=3)
schedule.add_lectures("segnali", ["GIO 15-19"], ["LabSS-LabAstro"], channel=1)
schedule.add_lectures("segnali", ["MAR 15-19"], ["LabSS-LabAstro"], channel=2)
schedule.add_lectures("segnali", ["LUN 15-19"], ["LabSS-LabAstro"], channel=3)

schedule.add_lectures("quantum", ["LUN 8-10"],["Aula3"], channel=1)
schedule.add_lectures("quantum", ["MAR 12-14", "GIO 13-14", "VEN 13-15"],["Aula6"], channel=1)

schedule.add_lectures("quantum", ["LUN 8-10", "MAR 12-14", "MER 14-15", "VEN 12-14"],["Aula4"], channel=2)

schedule.add_lectures("quantum", ["LUN 8-10", "MER 16-17"],["Aula6"], channel=3)
schedule.add_lectures("quantum", ["MAR 12-14", "VEN 12-14"],["Aula3"], channel=3)

schedule.add_lectures("statmech", ["LUN 12-14"], ["Aula3"], channel=1)
schedule.add_lectures("statmech", ["MAR 14-16", "VEN 12-13"], ["Aula6"], channel=1)

schedule.add_lectures("statmech", ["LUN 12-14", "MER 15-17", "VEN 14-15"], ["Aula4"], channel=2)

schedule.add_lectures("statmech", ["LUN 12-14", "MER 14-16"], ["Aula6"], channel=3)
schedule.add_lectures("statmech", ["VEN 15-16"], ["Aula4"], channel=3)

# T3.A
schedule.add_lectures("astro", ["LUN 16-18", "MER 10-11", "GIO 8-10"], ["Amaldi"])
schedule.add_lectures("quantstat", ["LUN 10-12", "MAR 10-13", "GIO 10-12", "VEN 12-14"], ["Careri"])
schedule.add_lectures("labastro", ["MAR 16-18", "VEN 10-12"], ["Careri"])
schedule.add_lectures("labastro", ["MER 15-19"], ["LabSS-LabAstro"], comment="all")
schedule.add_lectures("astrofluo", ["LUN 14-16", "MER 11-12", "VEN 8-10"], ["Amaldi"])

# T3.O
schedule.add_lectures("prob", ["GIO 12-13", "MAR 8-10", "MER 8-10"], ["Amaldi"])

# M1
schedule.add_lectures("rqm.1", ["MAR 14-16"], ["Aula7"])
schedule.add_lectures("rqm.1", ["GIO 14-16"], ["Aula6"])
schedule.add_lectures("rqm.1", ["VEN 8-9"], ["Conversi"])

schedule.add_lectures("rqm.2", ["MAR 14-16"], ["Conversi"])
schedule.add_lectures("rqm.2", ["GIO 14-16"], ["Conversi"])
schedule.add_lectures("rqm.2", ["VEN 8-9"], ["Majorana"])

schedule.add_lectures("condmat.1", ["MAR 16-18", "MER 16-17", "VEN 16-18"], ["Aula7"])
schedule.add_lectures("condmat.2", ["MAR 16-18", "MER 16-17", "VEN 16-18"], ["Amaldi"])

schedule.add_lectures("critical", ["LUN 11-12"], ["Rasetti"])
schedule.add_lectures("critical", ["MAR 12-14"], ["Conversi"])
schedule.add_lectures("critical", ["GIO 16-18"], ["Careri"])

schedule.add_lectures("generale", ["LUN 12-14", "MER 17-18", "VEN 14-16"], ["Amaldi"])

schedule.add_lectures("bio", ["MAR 14-16"], ["Aula5"])
schedule.add_lectures("bio", ["LUN 8-9", "GIO 8-10"], ["Majorana"])

schedule.add_lectures("physlab.1", ["MAR 10-12"], ["Conversi"])
schedule.add_lectures("physlab.1", ["LUN 16-18"], ["Rasetti"])

schedule.add_lectures("physlab.2", ["VEN 10-12"], ["Conversi"])
schedule.add_lectures("physlab.2", ["MER 14-16"], ["Careri"])

schedule.add_lectures("physlab.3", ["MAR 10-12"], ["Rasetti"])
schedule.add_lectures("physlab.3", ["LUN 16-18"], ["Careri"])

schedule.add_lectures("compmet.1", ["LUN 8-11"], ["LabCalc"])
schedule.add_lectures("compmet.1", ["MAR 8-10"], ["Majorana"])
schedule.add_lectures("compmet.1", ["GIO 12-14"], ["Conversi"])

schedule.add_lectures("compmet.2", ["LUN 12-16", "VEN 9-11"], ["LabCalc"])

schedule.add_lectures("compmet.3", ["MER 8-10", "VEN 9-10"], ["Careri"])
schedule.add_lectures("compmet.3", ["GIO 12-14"], ["Majorana"])

schedule.add_lectures("compmet.4", ["MER 14-16"], ["Conversi"])
schedule.add_lectures("compmet.4", ["LUN 14-16"], ["Conversi"])
schedule.add_lectures("compmet.4", ["VEN 11-14"], ["LabCalc"])

schedule.add_lectures("softmatter", ["LUN 10-11", "MER 12-14", "GIO 12-14"], ["Rasetti"])

schedule.add_lectures("nlquantumoptics", ["LUN 9-10", "MER 10-12", "GIO 10-12"], ["Conversi"])

schedule.add_lectures("superiore", ["MAR 10-11"], ["Majorana"])
schedule.add_lectures("superiore", ["MER 10-12", "GIO 8-10"], ["Rasetti"])

schedule.add_lectures("processi", ["GIO 10-12", "VEN 13-14"], ["Rasetti"])
schedule.add_lectures("processi", ["MER 12-14"], ["Conversi"])

schedule.add_lectures("astrolab", ["MAR 16-18", "GIO 16-18", "VEN 12-13"], ["Rasetti"])

schedule.add_lectures("biochem", ["MAR 8-10"], ["Careri"])
schedule.add_lectures("biochem", ["GIO 14-15", "VEN 14-16"], ["Rasetti"])

# M2
schedule.add_lectures("complex", ["LUN 12-14"], ["Rasetti"])
schedule.add_lectures("complex", ["MER 12-13", "GIO 14-16"], ["Careri"])

schedule.add_lectures("quantinfo", ["MAR 8-10"], ["Rasetti"])
schedule.add_lectures("quantinfo", ["MER 13-14", "GIO 12-14"], ["Careri"])
schedule.add_lectures("stoca", ["LUN 14-16", "MER 16-18"], ["Rasetti"])
schedule.add_lectures("stoca", ["VEN 15-16"], ["Careri"])
schedule.add_lectures("quantumfield", ["LUN 8-10", "GIO 15-16", "VEN 8-10"], ["Rasetti"])

schedule.add_lectures("disordered", ["LUN 10-12"], ["Conversi"])
schedule.add_lectures("disordered", ["MER 14-15", "VEN 10-12"], ["Rasetti"])

schedule.add_lectures("manybody", ["MAR 14-15"], ["Amaldi"])
schedule.add_lectures("manybody", ["MER 10-12", "GIO 8-10"], ["Careri"])

schedule.add_lectures("surface", ["GIO 10-12"], ["LabCalc"])
schedule.add_lectures("surface", ["MER 9-10"], ["Rasetti"])
schedule.add_lectures("surface", ["MAR 16-18"], ["Aula6"])

schedule.add_lectures("weak", ["LUN 14-16", "MER 16-18", "VEN 14-15"], ["Careri"])

schedule.add_lectures("expgrav", ["LUN 14-16", "MER 10-11", "VEN 8-10"], ["Aula5"])

schedule.add_lectures("intrograv", ["MAR 8-10", "GIO 12-14"], ["Aula5"])
schedule.add_lectures("intrograv", ["MER 15-16"], ["Aula8"])

schedule.add_lectures("labcalcadv", ["MAR 10-12"], ["Aula8"])
schedule.add_lectures("labcalcadv", ["VEN 10-12"], ["Aula5"])
schedule.add_lectures("labcalcadv", ["MAR 12-14"], ["Lab1MAT"])

schedule.add_lectures("spaceastro", ["MAR 14-16"], ["Rasetti"])
schedule.add_lectures("spaceastro", ["LUN 12-14"], ["Careri"])
schedule.add_lectures("spaceastro", ["GIO 11-12"], ["Majorana"])

schedule.add_lectures("astropart", ["LUN 8-10", "VEN 12-14"], ["Aula8"])

schedule.add_lectures("evoluzione", ["MER 16-18"], ["Aula8"])
schedule.add_lectures("evoluzione", ["VEN 14-16"], ["Aula8"])

schedule.add_lectures("current", ["LUN 12-14", "MAR 11-12"], ["Aula5"])
schedule.add_lectures("current", ["GIO 10-12"], ["Aula8"])

schedule.add_lectures("medical", ["MER 14-16", "GIO 16-18", "VEN 16-18"], ["Aula5"])

schedule.add_lectures("intropart", ["LUN 16-18", "MAR 12-14", "MER 9-10"], ["Aula5"])

schedule.add_lectures("autograv", ["MAR 16-18", "GIO 16-18"], ["Aula8"])

schedule.add_lectures("cosmo", ["LUN 16-18"], ["Aula7"])
schedule.add_lectures("cosmo", ["VEN 16-18"], ["Careri"])

schedule.add_lectures("collider", ["MAR 14-16", "MER 13-15", "GIO 8-10"], ["Aula8"])

schedule.add_lectures("solidsensors", ["LUN 8-10"], ["Careri"])
schedule.add_lectures("solidsensors", ["MAR 12-14"], ["Aula8"])
schedule.add_lectures("solidsensors", ["VEN 11-12"], ["Aula8"])

schedule.add_lectures("solids", ["MAR 12-14"], ["Rasetti"])
schedule.add_lectures("solids", ["VEN 12-15"], ["Conversi"])

# impegni non didattici
schedule.add_lectures("seminari", ["LUN 16-18", "MAR 16-18", "MER 16-18", "GIO 16-18", "VEN 16-18"], ["Conversi"])

schedule.add_lectures("studenti", ["LUN 14-19", "MAR 14-19", "MER 14-19", "VEN 14-19"], ["Majorana"])
schedule.add_lectures("studenti", ["GIO 16-19"], ["Majorana"])

schedule.add_lectures("CdD/CAD", ["GIO 14-19"], ["Amaldi"])
schedule.add_lectures("dottorato", ["LUN 8-19", "MAR 8-19", "MER 8-19", "GIO 8-19", "VEN 8-19"], ["Aula2"])
schedule.add_lectures("Chimica", ["LUN 14-18", "MAR 14-18", "MER 14-18", "GIO 14-18", "VEN 14-18"], ["Aula3"])
schedule.add_lectures("Genetica", ["LUN 16-18", "MAR 14-18", "GIO 14-18", "VEN 16-18"], ["Aula4"])
schedule.add_lectures("gravity_meeting", ["MER 15-16"], ["Rasetti"])
schedule.add_lectures("ScienzeAmbientali", ["MAR 14-16"], ["Careri"])
schedule.add_lectures("ScienzeAmbientali", ["GIO 14-16"], ["Majorana"])

schedule.export_html("2023/PRIMO_SEMESTRE/v1.0_orario.html", provisional=True)
schedule.report("2023/PRIMO_SEMESTRE/v1.0_report.html")
schedule.simulation("2023/PRIMO_SEMESTRE/v1.0_simulazione.html")
