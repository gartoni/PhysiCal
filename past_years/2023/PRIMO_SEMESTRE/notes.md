## Vincoli

* Laboratorio di Calcolo, canale 2:
  * Esercitazioni LUN 13-16 da tenere per Shahram
  * Esercitazioni GIO 14-17 OK ma si può anche spostare a LUN 16-19
  * No lezione MAR e MER perché Livia una settimana su due sarà al CERN
  
* Fisica dei sistemi complessi
  * Mettere le lezioni in due giorni vicini (LUN/MAR o GIO/VEN)
  * Mettere una delle due lezioni con orario 16-19 
  
## v 3.4

* Analisi (Pinzari), Martedì Aula 4, 8-10 -> 10-12
* Geometria (Salvati Manni), Martedì Aula 4, 10-12 -> 8-10
* Statistical Mechanics of Disordered Systems (Ricci Tersenghi), Lunedì e Venerdì, 10-12 -> 9-12
* Astronomia (Melchiorri), Martedì 12-14 e Mercoledì 8-10 cambio aula Conversi -> Amaldi

## v 3.5

* Introduction to Quantum Field Theory (K-Z) (Bonciani)
  * Martedì 14-16 da Conversi -> Aula 7
  * Giovedì 14-16 da Conversi -> Aula 7
  * Venerdì 8-9 da Conversi -> Aula 7
* Meccanica Statistica (Giardina), Martedì 14-16 da Aula 7 -> Conversi
* Fluidodinamica per l'Astrofisica (Capuzzo Dolcetta), Giovedì 14-15 da Aula 7 -> Amaldi
* Physics Laboratory HEP (Cavoto)
  * Mercoledì 14-16 da Rasetti -> Careri
  * Venerdì 10-12 da Rasetti -> Majorana
* Statistical Mechanics of Disordered Systems (Ricci Tersenghi), Mercoledì 14-15 da Careri -> Rasetti
* Solid State Sensors (Polimeni), Venerdì 11-12 da Majorana -> Rasetti
* Statistical Physics and Machine Learning (Cammarota), Mercoledì 15-16 da Careri -> Rasetti
* General Relativity (Pani), Mercoledì da 17-18 -> 16-17
* Condensed Matter Physics (K-Z) (Polimeni), Mercoledì da 16-17 -> 17-18 (sempre in 3)
* Condensed Matter Physics (K-Z) (Caprara), Mercoledì da Aula 4 16-17 -> Cabibbo 17-18
* Condensed Matter Physics (K-Z) (Caprara), Venerdì 16-18 da Aula 4 -> Cabibbo
* Dottorato, Giovedì Aula 7, da 15-19 -> 16-19
* CdD, Giovedì Amaldi, da 14-19 -> 15-19
* CdD, Giovedì Conversi 14-16

## v 3.6

* Cambio di aula tra il I e il III canale del II anno:
  * Il I canale (Lanzara, Saini, Crisanti, Ricci Tersenghi) va in Aula 7
  * Il III canale (Terracina, Rapagnani, Bonvini, Marinari) va in Amaldi
  * Gli orari delle esercitazioni di laboratorio restano invariati