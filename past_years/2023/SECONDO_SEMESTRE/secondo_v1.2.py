'''
Created on Jun 16, 2021

@author: lorenzo
'''

import datetime as dt
from physical import physical

schedule = physical.Schedule("lectures_data.xlsx", semester=2, semester_start=dt.date(2023, 2, 23), semester_end=dt.date(2023, 6, 13))

# holidays
schedule.add_holidays("2023-04-24")
schedule.add_holidays("2023-04-25")
schedule.add_holidays("2023-05-01")
schedule.add_holidays("2023-06-02")
schedule.add_holidays("2023-04-06", "2023-04-11")

# pauses

# T1
schedule.add_lectures("mec", ["LUN 8-10", "MAR 14-16", "MER 10-11", "GIO 16-18", "VEN 12-14"], ["Aula3"], channel=1)
schedule.add_lectures("mec", ["LUN 14-16", "MAR 8-10", "MER 15-16", "GIO 10-12", "VEN 8-10"], ["Aula3"], channel=2)
schedule.add_lectures("mec", ["LUN 14-16", "MAR 8-10", "MER 15-16", "GIO 16-18", "VEN 8-10"], ["Aula4"], channel=3)
schedule.add_lectures("mec", ["LUN 10-12", "MAR 14-16", "MER 11-12", "GIO 10-12", "VEN 12-14"], ["Aula4"], channel=4)

schedule.add_lectures("chem", ["LUN 10-12", "MER 11-12", "GIO 14-16"], ["Aula3"], channel=1)
schedule.add_lectures("chem", ["LUN 16-18", "MER 14-15", "GIO 8-10"], ["Aula3"], channel=2)
schedule.add_lectures("chem", ["MER 14-15", "GIO 14-16", "VEN 10-12"], ["Aula4"], channel=3)
schedule.add_lectures("chem", ["LUN 8-10", "MER 10-11", "GIO 8-10"], ["Aula4"], channel=4)

schedule.add_lectures("labmec", ["MAR 16-18", "MER 8-10", "VEN 14-16"], ["Aula3"], channel=1)
schedule.add_lectures("labmec", ["MAR 10-12", "MER 12-14", "VEN 10-12"], ["Aula3"], channel=2)
schedule.add_lectures("labmec", ["LUN 16-18", "MAR 10-12", "MER 12-14"], ["Aula4"], channel=3)
schedule.add_lectures("labmec", ["MAR 16-18", "MER 8-10", "VEN 14-16"], ["Aula4"], channel=4)

# T2
schedule.add_lectures("metodi", ["LUN 14-16", "MAR 8-10", "MER 15-16", "GIO 14-16", "VEN 8-10"], ["Aula7"], channel=1)
schedule.add_lectures("metodi", ["LUN 10-12", "MAR 8-10", "MER 15-16", "GIO 14-16", "VEN 8-10"], ["Aula6"], channel=2)
schedule.add_lectures("metodi", ["LUN 14-16", "MAR 8-10", "MER 16-18", "GIO 9-10", "VEN 8-10"], ["Amaldi"], channel=3)

# i canali 1 e 2 hanno un'ora in più dietro richiesta dei docenti ma per evitare che figuri sull'orario
# mettiamo il generico "impegni didattici" per bloccare l'aula
schedule.add_lectures("em", ["LUN 16-18", "MAR 10-12", "MER 14-15", "GIO 12-14", "VEN 10-12"], ["Aula7"], channel=1)
schedule.add_lectures("didattica", ["VEN 12-13"], ["Aula7"], channel=1)
schedule.add_lectures("em", ["LUN 8-10", "MAR 10-12", "MER 14-15", "GIO 12-14", "VEN 10-12"], ["Aula6"], channel=2)
schedule.add_lectures("didattica", ["VEN 12-13"], ["Aula6"], channel=1)
schedule.add_lectures("em", ["LUN 16-18", "MAR 10-12", "MER 14-16", "GIO 8-9", "VEN 10-12"], ["Amaldi"], channel=3)

schedule.add_lectures("labem", ["MAR 12-13", "MER 16-18"], ["Aula7"], channel=1)
schedule.add_lectures("labem", ["MAR 12-13", "MER 16-18"], ["Aula6"], channel=2)
schedule.add_lectures("labem", ["MAR 12-13", "GIO 10-12"], ["Amaldi"], channel=3)

schedule.add_lectures("labem", ["MAR 15-19"], ["LabEm"], channel=1)
schedule.add_lectures("labem", ["GIO 15-19"], ["LabEm"], channel=3)
schedule.add_lectures("labem", ["LUN 15-19"], ["LabEm"], channel=2)

# T2.A
schedule.add_lectures("metodi.astro", ["MAR 8-10", "MER 13-14", "GIO 10-12", "VEN 8-10"], ["Conversi"])
schedule.add_lectures("eng", ["LUN 12-14"], ["Rasetti"])
schedule.add_lectures("eng", ["VEN 14-16"], ["Majorana"])
schedule.add_lectures("eng", ["VEN 16-18"], ["Aula6"])

# T3
schedule.add_lectures("ottica", ["LUN 10-12", "MAR 16-18", "MER 13-14"], ["Amaldi"], channel=1)
schedule.add_lectures("ottica", ["LUN 16-18", "MER 13-14", "GIO 8-10"], ["Aula6"], channel=2)
schedule.add_lectures("ottica", ["LUN 10-12", "GIO 8-9", "VEN 13-15"], ["Aula7"], channel=3)

schedule.add_lectures("struttura", ["MER 10-12", "VEN 13-16"], ["Amaldi"], channel=1)
schedule.add_lectures("struttura", ["LUN 14-16", "MER 10-12", "VEN 15-16"], ["Aula6"], channel=2)
schedule.add_lectures("struttura", ["LUN 8-10", "MER 10-12", "GIO 9-10"], ["Aula7"], channel=3)

schedule.add_lectures("nucleare", ["LUN 8-10", "MAR 14-16", "MER 12-13"], ["Amaldi"], channel=1)
schedule.add_lectures("nucleare", ["MER 12-13", "GIO 10-12", "VEN 13-15"], ["Aula6"], channel=2)
schedule.add_lectures("nucleare", ["MER 12-14", "GIO 10-12", "VEN 15-16"], ["Aula7"], channel=3)

schedule.add_lectures("ottica", ["LUN 15-19"], ["LabOtt"], channel=1)
schedule.add_lectures("ottica", ["MAR 15-19", "MER 15-19"], ["LabOtt"], channel=2)
schedule.add_lectures("ottica", ["GIO 15-19"], ["LabOtt"], channel=3)

# T3.O
schedule.add_lectures("metodi_IA", ["GIO 12-14 1-7"], ["Aula3"])
schedule.add_lectures("metodi_IA", ["MER 8-10"], ["Aula6"])
schedule.add_lectures("metodi_IA", ["GIO 12-14 8-end"], ["LabSS-LabAstro"])

schedule.add_lectures("applicata", ["LUN 12-14", "VEN 10-13"], ["LabCalc"])

schedule.add_lectures("atmosfera", ["MAR 8-10", "VEN 8-10"], ["Rasetti"])

schedule.add_lectures("genevo", ["MAR 12-14", "VEN 16-18"], ["Aula3"])

schedule.add_lectures("elettronica", ["MAR 10-12", "MER 16-18", "GIO 14-15"], ["Majorana"])

# M1
schedule.add_lectures('spectra',["MAR 12-14", "GIO 10-12"], ["Majorana"])
schedule.add_lectures('spectra',["VEN 9-10 11-14"], ["Careri"])

schedule.add_lectures('cond',["LUN 12-14", "VEN 12-13"], ["Amaldi"])
schedule.add_lectures('cond',["MER 10-12"], ["Rasetti"])

schedule.add_lectures('particles',["LUN 12-14", "MER 10-12"], ["Conversi"])
schedule.add_lectures('particles',["VEN 9-10"], ["Majorana"])

schedule.add_lectures('matphys',["MER 14-16", "GIO 8-10"], ["Conversi"])

schedule.add_lectures('neq',["MAR 16-18", "LUN 16-19"], ["Careri"])

schedule.add_lectures('gravity',["LUN 14-16", "MER 8-10", "GIO 14-15"], ["Careri"])

schedule.add_lectures('nuclear',["VEN 12-14", "MER 8-10"], ["Rasetti"])

schedule.add_lectures('liquids',["MER 8-10", "VEN 10-12"], ["Conversi"])

schedule.add_lectures('neural',["LUN 14-16", "MER 12-14"], ["Rasetti"])
schedule.add_lectures('symm',["MER 16-18", "VEN 10-12"], ["Careri"])

schedule.add_lectures('molbio',["MAR 10-12", "GIO 12-14"], ["Seminari"])

schedule.add_lectures('exppart',["LUN 16-18", "MER 18-19"], ["Majorana"])
schedule.add_lectures('exppart',["VEN 16-18"], ["Majorana"])

schedule.add_lectures('labphys2bio',["LUN 10-12 1-7", "GIO 16-19 1-7", "VEN 9-10 1-7"], ["Careri"])

schedule.add_lectures('labphys2mat',["LUN 10-12 1-5"], ["Majorana"])
schedule.add_lectures('labphys2mat',["GIO 16-19 1-5"], ["Rasetti"])

schedule.add_lectures('labphys2part',["GIO 16-19"], ["Aula7"])
schedule.add_lectures('labphys2part',["VEN 14-16"], ["Conversi"])

schedule.add_lectures('astroalte',["MER 16-18"], ["Aula4"])
schedule.add_lectures('astroalte',["VEN 16-18"], ["Aula7"])

schedule.add_lectures('extra',["LUN 12-14", "MAR 14-16"], ["Aula7"])

schedule.add_lectures('astrotheo',["LUN 10-12", "GIO 10-12"], ["Rasetti"])

schedule.add_lectures('stellare',["GIO 12-14"], ["Amaldi"])
schedule.add_lectures('stellare',["VEN 12-14"], ["Careri"])

schedule.add_lectures('astropt',["MAR 12-14", "MER 10-12"], ["Careri"])

schedule.add_lectures('stardyn',["MAR 16-18", "GIO 16-18"], ["Majorana"])

schedule.add_lectures('obscosmo',["MER 12-14", "VEN 10-12"], ["Majorana"])

schedule.add_lectures('physcosmo',["MAR 10-12", "GIO 15-16"], ["Careri"])
schedule.add_lectures('physcosmo',["MER 14-16"], ["LabCalc"])

schedule.add_lectures('planets',["LUN 16-18", "VEN 14-16"], ["LabCalc"])

# usiamo check_teacher_overlap per evitare che la dicitura "TBA" dia problemi
schedule.add_lectures('eng2',["MER 8-11 12-end"], ["Majorana"], check_teacher_overlap=False)
schedule.add_lectures('eng2',["VEN 17-19"], ["Amaldi"], check_teacher_overlap=False)

schedule.add_lectures('detnacc',["MAR 10-12", "GIO 14-16"], ["Conversi"])

schedule.add_lectures('compuarch',["LUN 14-16", "GIO 12-14"], ["Majorana"])

schedule.add_lectures('bioteo',["MAR 8-10", "VEN 14-16"], ["Careri"])

schedule.add_lectures('phot',["MAR 16-18", "GIO 14-16"], ["Rasetti"])

schedule.add_lectures('nonlinear',["GIO 12-14", "VEN 16-18"], ["Careri"])

schedule.add_lectures('biophys',["MAR 12-14", "VEN 16-18"], ["Aula4"])

schedule.add_lectures('advanced_ML',["MAR 14-16"], ["Aula6"])
schedule.add_lectures('advanced_ML',["MER 16-18 1-7"], ["Aula3"])
schedule.add_lectures('advanced_ML',["MER 16-18 8-end"], ["LabSS-LabAstro"])

schedule.add_lectures('fundamental',["LUN 8-10", "MAR 12-14", "MER 12-13"], ["Conversi"])

# impegni non didattici
schedule.add_lectures("seminari", ["LUN 16-18", "MAR 16-18", "MER 16-18", "GIO 16-18", "VEN 16-18"], ["Conversi"])
schedule.add_lectures("studenti", ["LUN 8-19", "MAR 8-19", "MER 8-19", "GIO 8-19", "VEN 8-19"], ["Aula8"])

schedule.add_lectures("CdD/CAD", ["GIO 14-19"], ["Amaldi"])
schedule.add_lectures("dottorato", ["LUN 8-19", "MAR 8-19", "MER 8-19", "GIO 8-19", "VEN 8-19"], ["Aula2"])
schedule.add_lectures("dottorato", ["LUN 8-19", "MAR 8-19", "MER 8-19", "GIO 8-19", "VEN 8-19"], ["Aula5"])
schedule.add_lectures("dottorato", ["MER 14-19", "VEN 14-19"], ["Rasetti"])
# schedule.add_lectures("Chimica", ["LUN 14-18", "MAR 14-18", "MER 14-18", "GIO 14-18", "VEN 14-18"], ["Aula3"])
# schedule.add_lectures("Genetica", ["LUN 16-18", "MAR 14-18", "GIO 14-18", "VEN 16-18"], ["Aula4"])
# schedule.add_lectures("ScienzeAmbientali", ["MAR 14-16"], ["Careri"])
# schedule.add_lectures("ScienzeAmbientali", ["GIO 14-16"], ["Majorana"])

version = "1.2"
schedule.export_html(f"2023/SECONDO_SEMESTRE/v{version}_orario.html", provisional=True)
schedule.report(f"2023/SECONDO_SEMESTRE/v{version}_report.html")
schedule.simulation(f"2023/SECONDO_SEMESTRE/v{version}_simulazione.html")
schedule.capacity_analysis(f"2023/SECONDO_SEMESTRE/v{version}_capacity.html")
schedule.capacity_analysis(f"2023/SECONDO_SEMESTRE/v{version}_capacity_extrapolated_3.html", years_forward=3)
