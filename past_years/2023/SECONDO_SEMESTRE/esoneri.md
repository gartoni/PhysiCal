* Un lunedì, un mercoledì, due venerdì
* Il lunedì:
  * Lab meccanica in aula 4 si sposta alle 12-14
  * Chimica in aula 3 non si tiene
  * Sciarrino in aula 6 non si tiene
  * In aula 7 e Amaldi c'è già lezione di elettromagnetismo quindi non servono cambi
* Il mercoledì:
  * Giagu in aula 3 ha finito
  * Pisano in aula 4 -> 2/5/8/Conversi
  * Lab di circuiti in 6 e 7 non si tiene
  * Modelli e metodi (Esposito) in Amaldi non si tiene
* Il venerdì:
  * Pisano in aula 6 -> 2/5/8
  * Di Leonardo in Conversi/Rasetti
  * Genetica ed evoluzione in Conversi/Rasetti
  