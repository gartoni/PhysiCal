'''
Created on Jun 16, 2021

@author: lorenzo
'''

import datetime as dt
from physical import physical

schedule = physical.Schedule("lectures_data.xlsx", semester=2, semester_start=dt.date(2022, 2, 23), semester_end=dt.date(2022, 6, 10))

# holidays
schedule.add_holidays("2022-04-25")
schedule.add_holidays("2022-05-01")
schedule.add_holidays("2022-06-02")
schedule.add_holidays("2022-04-14", "2022-04-19")

# T1
schedule.add_lectures("mec", ["LUN 8-10", "MAR 14-16", "MER 11-12", "GIO 10-12", "VEN 12-14"], ["Aula3"], channel=1)
schedule.add_lectures("mec", ["LUN 14-16", "MAR 8-10", "MER 15-16", "GIO 16-18", "VEN 8-10"], ["Aula3"], channel=2)
schedule.add_lectures("mec", ["LUN 10-12", "MAR 14-16", "MER 11-12", "GIO 10-12", "VEN 12-14"], ["Aula4"], channel=3)
schedule.add_lectures("mec", ["LUN 14-16", "MAR 8-10", "MER 15-16", "GIO 16-18", "VEN 8-10"], ["Aula4"], channel=4)

schedule.add_lectures("chem", ["LUN 10-12", "MER 10-11", "GIO 8-10"], ["Aula3"], channel=1)
schedule.add_lectures("chem", ["LUN 16-18", "MER 14-15", "GIO 14-16"], ["Aula3"], channel=2)
schedule.add_lectures("chem", ["LUN 8-10", "MER 10-11", "GIO 8-10"], ["Aula4"], channel=3)
schedule.add_lectures("chem", ["LUN 16-18", "MER 14-15", "GIO 14-16"], ["Aula4"], channel=4)

schedule.add_lectures("labmec", ["MAR 16-18", "MER 8-10", "VEN 14-16"], ["Aula3"], channel=1)
schedule.add_lectures("labmec", ["MAR 10-12", "MER 12-14", "VEN 10-12"], ["Aula3"], channel=2)
schedule.add_lectures("labmec", ["MAR 16-18", "MER 8-10", "VEN 14-16"], ["Aula4"], channel=3)
schedule.add_lectures("labmec", ["MAR 10-12", "MER 12-14", "VEN 10-12"], ["Aula4"], channel=4)

# T2
schedule.add_lectures("metodi", ["LUN 14-16", "MAR 8-10", "MER 15-16", "GIO 10-12", "VEN 8-10"], ["Amaldi"], channel=1)
schedule.add_lectures("metodi", ["LUN 10-12", "MAR 8-10", "MER 15-16", "GIO 14-16", "VEN 8-10"], ["Aula6"], channel=2)
schedule.add_lectures("metodi", ["LUN 14-16", "MAR 8-10", "MER 15-16", "GIO 14-16", "VEN 8-10"], ["Aula7"], channel=3)

schedule.add_lectures("em", ["LUN 16-18", "MAR 10-12", "MER 14-15", "GIO 8-10", "VEN 10-12"], ["Amaldi"], channel=1)
schedule.add_lectures("em", ["LUN 8-10", "MAR 10-12", "MER 14-15", "GIO 12-14", "VEN 10-12"], ["Aula6"], channel=2)
schedule.add_lectures("em", ["LUN 16-18", "MAR 10-12", "MER 14-15", "GIO 12-14", "VEN 10-12"], ["Aula7"], channel=3)

schedule.add_lectures("labem", ["MAR 12-13", "MER 16-18"], ["Amaldi", "Aula6", "Aula7"])

schedule.add_lectures("labem", ["GIO 15-19"], ["LabEm"], channel=1)
schedule.add_lectures("labem", ["LUN 15-19"], ["LabEm"], channel=2)
schedule.add_lectures("labem", ["MAR 15-19"], ["LabEm"], channel=3)

# T2.A
schedule.add_lectures("metodi.astro", ["MAR 8-10", "MER 13-14", "GIO 10-12", "VEN 8-10"], ["Conversi"])
schedule.add_lectures("eng", ["VEN 13-16"], ["Conversi"])

# T3
schedule.add_lectures("ottica", ["LUN 10-12", "MAR 16-18", "MER 9-10"], ["Amaldi"], channel=1)
schedule.add_lectures("ottica", ["LUN 16-18", "MER 9-10", "GIO 8-10"], ["Aula6"], channel=2)
schedule.add_lectures("ottica", ["LUN 10-12", "GIO 8-10", "VEN 15-16"], ["Aula7"], channel=3)

schedule.add_lectures("struttura", ["LUN 8-10", "MER 10-12", "VEN 15-16"], ["Amaldi"], channel=1)
schedule.add_lectures("struttura", ["LUN 14-16", "MER 10-12", "VEN 15-16"], ["Aula6"], channel=2)
schedule.add_lectures("struttura", ["LUN 8-10", "MER 10-12", "GIO 10-11"], ["Aula7"], channel=3)

schedule.add_lectures("nucleare", ["MAR 14-16", "MER 8-9", "VEN 13-15"], ["Amaldi"], channel=1)
schedule.add_lectures("nucleare", ["MER 8-9", "GIO 10-12", "VEN 13-15"], ["Aula6"], channel=2)
schedule.add_lectures("nucleare", ["MER 8-10", "GIO 11-12", "VEN 13-15"], ["Aula7"], channel=3)

schedule.add_lectures("ottica", ["LUN 15-19"], ["LabOtt"], channel=1)
schedule.add_lectures("ottica", ["MAR 15-19", "MER 15-19"], ["LabOtt"], channel=2)
schedule.add_lectures("ottica", ["GIO 15-19"], ["LabOtt"], channel=3)

# T3.O
schedule.add_lectures("metodi_IA", ["GIO 12-14 1-7"], ["Aula3"])
schedule.add_lectures("metodi_IA", ["MER 12-14"], ["Aula6"])
schedule.add_lectures("metodi_IA", ["GIO 12-14 8-end"], ["LabSS-LabAstro"])

schedule.add_lectures("applicata", ["LUN 12-14", "VEN 10-13"], ["LabCalc"])

schedule.add_lectures("atmosfera", ["MAR 8-10", "VEN 8-10"], ["Majorana"])

schedule.add_lectures("genevo", ["MAR 12-14", "VEN 16-18"], ["Aula3"])

schedule.add_lectures("elettronica", ["MAR 10-12", "MER 16-18", "GIO 14-15"], ["Aula8"])

# M1
schedule.add_lectures('spectra',["MAR 12-14", "GIO 10-12"], ["Majorana"])
schedule.add_lectures('cond',["LUN 12-14", "MER 12-14", "VEN 12-13"], ["Aula7"])
schedule.add_lectures('particles',["LUN 12-14", "MER 10-12", "MAR 16-17"], ["Rasetti"])

schedule.add_lectures('qed',["MAR 12-14", "MER 10-12", "VEN 12-13"], ["Conversi"])

schedule.add_lectures('matphys',["MER 14-16", "GIO 8-10"], ["Conversi"])
schedule.add_lectures('group',["GIO 12-14", "VEN 8-10"], ["Rasetti"])

schedule.add_lectures('neq',["MAR 16-18", "LUN 16-19"], ["Careri"])
schedule.add_lectures('gravity',["LUN 14-16", "MER 8-10"], ["Careri"])
schedule.add_lectures('super',["LUN 8-10", "MER 10-12", "VEN 13-14"], ["Majorana"])
schedule.add_lectures('nuclear',["VEN 12-14", "MER 8-10"], ["Rasetti"])

schedule.add_lectures('liquids',["MER 8-10", "VEN 10-12"], ["Conversi"])

schedule.add_lectures('neural',["LUN 14-16", "MER 12-14"], ["Rasetti"])
schedule.add_lectures('symm',["MER 16-18", "VEN 10-12"], ["Careri"])

schedule.add_lectures('molbio',["MAR 10-12", "GIO 12-14"], ["Seminari"])

schedule.add_lectures('exppart',["LUN 16-18", "MER 18-19", "VEN 14-16"], ["Aula8"])

schedule.add_lectures('labphys2bio',["LUN 10-12 1-7", "GIO 16-19 1-7", "VEN 9-10 1-7"], ["Careri"])

schedule.add_lectures('labphys2mat',["LUN 10-12 1-5"], ["Majorana"])
schedule.add_lectures('labphys2mat',["GIO 16-19 1-5"], ["Rasetti"])

schedule.add_lectures('labphys2part',["GIO 16-19", "VEN 16-19 1-4"], ["Aula7"])

schedule.add_lectures('astroalte',["MAR 8-10", "GIO 8-10"], ["Rasetti"])

schedule.add_lectures('extra',["MAR 14-16"], ["Aula7"])
schedule.add_lectures('extra',["MER 16-18"], ["Aula4"])

schedule.add_lectures('astrotheo',["LUN 10-12", "GIO 10-12"], ["Rasetti"])

schedule.add_lectures('stellare',["LUN 12-14", "GIO 12-14"], ["Amaldi"])

schedule.add_lectures('astropt',["MAR 12-14", "MER 10-12"], ["Aula8"])

schedule.add_lectures('stardyn',["MAR 16-18", "GIO 16-18"], ["Aula8"])

schedule.add_lectures('obscosmo',["MER 12-14", "VEN 10-12"], ["Majorana"])

schedule.add_lectures('physcosmo',["MAR 10-12"], ["Careri"])
schedule.add_lectures('physcosmo',["MER 14-16", "GIO 15-16"], ["Aula8"])

schedule.add_lectures('planets',["LUN 16-18", "VEN 15-17"], ["LabCalc"])

schedule.add_lectures('eng2',["LUN 9-12", "LUN 13-16"], ["Conversi"])
schedule.add_lectures('detnacc',["MAR 10-12", "GIO 14-16"], ["Conversi"])

schedule.add_lectures('compuarch',["LUN 8-10", "MAR 12-14"], ["Rasetti"])

schedule.add_lectures('bioteo',["MAR 8-10", "VEN 14-16"], ["Careri"])

schedule.add_lectures('phot',["MAR 16-18"], ["Aula6"])
schedule.add_lectures('phot',["GIO 14-16"], ["Rasetti"])

schedule.add_lectures('nonlinear',["MAR 14-16", "VEN 10-12"], ["Rasetti"])
schedule.add_lectures('biophys',["MAR 12-14", "VEN 16-18"], ["Aula4"])

schedule.add_lectures('advanced_ML',["MAR 14-16"], ["Aula6"])
schedule.add_lectures('advanced_ML',["MER 16-18 1-7"], ["Aula3"])
schedule.add_lectures('advanced_ML',["MER 16-18 8-end"], ["LabSS-LabAstro"])

schedule.add_lectures('weak',["LUN 8-10", "MER 14-15", "GIO 10-12"], ["Careri"])


# impegni non didattici
schedule.add_lectures("seminari", ["LUN 16-18", "MAR 16-18", "MER 16-18", "GIO 16-18", "VEN 16-18"], ["Conversi"])
schedule.add_lectures("studenti", ["LUN 14-19", "MAR 14-19", "MER 14-19", "GIO 14-19", "VEN 14-19"], ["Majorana"])

schedule.add_lectures("CdD/CAD", ["GIO 14-19"], ["Amaldi"])
schedule.add_lectures("dottorato", ["LUN 8-19", "MAR 8-19", "MER 8-19", "GIO 8-19", "VEN 8-19"], ["Aula2"])
schedule.add_lectures("dottorato", ["LUN 8-19", "MAR 8-19", "MER 8-19", "GIO 8-19", "VEN 8-19"], ["Aula5"])
schedule.add_lectures("dottorato", ["MER 14-19", "VEN 14-19"], ["Rasetti"])
# schedule.add_lectures("Chimica", ["LUN 14-18", "MAR 14-18", "MER 14-18", "GIO 14-18", "VEN 14-18"], ["Aula3"])
# schedule.add_lectures("Genetica", ["LUN 16-18", "MAR 14-18", "GIO 14-18", "VEN 16-18"], ["Aula4"])
# schedule.add_lectures("ScienzeAmbientali", ["MAR 14-16"], ["Careri"])
# schedule.add_lectures("ScienzeAmbientali", ["GIO 14-16"], ["Majorana"])

schedule.export_html("2022/SECONDO_SEMESTRE/v2.2_orario.html", provisional=True)
schedule.report("2022/SECONDO_SEMESTRE/v2.2_report.html")
schedule.simulation("2022/SECONDO_SEMESTRE/v2.2_simulazione.html")
