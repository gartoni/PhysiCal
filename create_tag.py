"""
Script to quickly create a new tag

@author: Giacomo Artoni <giacomo.artoni@uniroma1.it>
"""
import os
import shutil
import sys


def main():
    version = 1.6
    semester = 2
    year = 2025

    tag_message = f'version {version}, semester {semester}, {year-1} - {year}'
    directory = f'past_years/{year}'
    tag = f's{semester}_{year-2001}{year-2000}-v{str(version).replace(".", "")}'
    tag_dir = os.path.join(directory, tag)
    os.mkdir(tag_dir)

    files = ["test_orario.html", "test_report.html", "test_simulazione_corsi.html", "test_simulazione_docenti.html"]
    for f in files:
        rep_list = [('.html', f'_{tag}.html'), ('simulazione_', ''), ('test_', '')]
        dest = os.path.join(tag_dir, replace_many(f, rep_list))
        shutil.copyfile(f, dest)
        print(f'git add {dest}')
    print(f'git commit -m "creating {tag} tag"')
    print(f'git tag -a {tag} -m "{tag_message}"')
    print(f'git push origin {tag}')


def replace_many(s, rep_list):
    for i, j in rep_list:
        s = s.replace(i, j)
    return s


if __name__ == '__main__':
    sys.exit(main())
