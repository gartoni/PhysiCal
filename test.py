"""
Work-in-progress calendar
Target: Second semester, A.A. 2024-2025

@author: Giacomo Artoni <giacomo.artoni@uniroma1.it>
"""
import sys
import datetime as dt
from physical import physical


def main():

    schedule = physical.Schedule(
        "lectures_data.xlsx", semester=2,
        semester_start=dt.date(2025, 2, 26),
        semester_end=dt.date(2025, 6, 13)
    )

    # Holidays
    schedule.add_holidays("2025-04-17", "2025-04-22")
    schedule.add_holidays("2025-04-25")
    schedule.add_holidays("2025-05-01")
    schedule.add_holidays("2025-06-02")

    # Requests!
    all_days = ['LUN', 'MAR', 'MER', 'GIO', 'VEN']

    #schedule.add_constraint(teacher='Paramatti', constraint=["MAR 8-19", "MER 8-19"])
    #schedule.add_constraint(teacher='Bovino', constraint=[f"{day} 8-9" for day in all_days])
    #schedule.add_constraint(teacher='Pettiti', constraint=[f"{day} 14-19" for day in all_days])
    #schedule.add_constraint(teacher='Cartoni', constraint=[f"{day} 14-19" for day in all_days])

    # T1
    schedule.add_lectures("mec", ["LUN 9-10", "MAR 14-16", "MER 10-12", "GIO 12-14", "VEN 12-14"], ["Aula4"], channel=1)
    schedule.add_lectures("didattica", ["LUN 8-9"], ["Aula4"], channel=1)
    schedule.add_lectures("mec", ["LUN 16-18", "MAR 9-10", "MER 14-16", "GIO 10-12", "VEN 8-10"], ["Aula4"], channel=2)
    schedule.add_lectures("didattica", ["MAR 8-9"], ["Aula4"], channel=1)
    schedule.add_lectures("mec", ["LUN 9-10", "MAR 8-10", "MER 12-14", "GIO 12-14", "VEN 8-10"], ["Cabibbo"], channel=3)
    schedule.add_lectures("didattica", ["LUN 8-9"], ["Cabibbo"], channel=1)
    schedule.add_lectures("mec", ["LUN 14-16", "MAR 14-16", "MER 10-12", "GIO 9-10", "VEN 12-14"], ["Cabibbo"], channel=4)
    schedule.add_lectures("didattica", ["GIO 8-9"], ["Cabibbo"], channel=1)

    schedule.add_lectures("chem", ["LUN 10-12", "MAR 13-14", "MER 8-10"], ["Aula4"], channel=1)
    schedule.add_lectures("didattica", ["MAR 12-13"], ["Aula4"], channel=1)
    schedule.add_lectures("chem", ["LUN 13-14", "MAR 10-12", "GIO 8-10"], ["Aula4"], channel=2)
    schedule.add_lectures("didattica", ["LUN 12-13"], ["Aula4"], channel=1)
    schedule.add_lectures("chem", ["MAR 12-13", "GIO 14-16", "VEN 10-12"], ["Cabibbo"], channel=3)
    schedule.add_lectures("didattica", ["MAR 13-14"], ["Cabibbo"], channel=1)
    schedule.add_lectures("chem", ["LUN 16-17", "MAR 16-18", "MER 8-10"], ["Cabibbo"], channel=4)
    schedule.add_lectures("didattica", ["LUN 17-18"], ["Cabibbo"], channel=1)

    schedule.add_lectures("labmec", ["MAR 16-18", "GIO 14-16", "VEN 14-16"], ["Aula4"], channel=1)
    schedule.add_lectures("labmec", ["LUN 14-16", "MER 12-14", "VEN 10-12"], ["Aula4"], channel=2)
    schedule.add_lectures("labmec", ["LUN 10-12", "MAR 10-12", "GIO 16-18"], ["Cabibbo"], channel=3)
    schedule.add_lectures("labmec", ["LUN 12-14", "GIO 10-12", "VEN 14-16"], ["Cabibbo"], channel=4)

    schedule.add_lectures("labmec", ["LUN 14-19"], ["LabMec"], channel=1)
    schedule.add_lectures("labmec", ["MAR 14-19"], ["LabMec"], channel=2)
    schedule.add_lectures("labmec", ["MER 14-19"], ["LabMec"], channel=3)
    schedule.add_lectures("labmec", ["GIO 14-19"], ["LabMec"], channel=4)

    # T2
    schedule.add_lectures("metodi", ["LUN 11-13", "MAR 8-10", "MER 15-16", "VEN 8-10"], ["Aula6"], channel=1)
    schedule.add_lectures("metodi", ["GIO 10-12"], ["Aula3"], channel=1)
    schedule.add_lectures("metodi", ["LUN 10-12", "MAR 16-18", "MER 8-10", "GIO 11-12", "VEN 13-15"], ["Amaldi"], channel=2)
    schedule.add_lectures("metodi", ["LUN 16-18", "MAR 8-10", "MER 16-18", "GIO 10-11", "VEN 8-10"], ["Amaldi"], channel=3)

    schedule.add_lectures("em", ["LUN 9-11", "MAR 10-12", "MER 14-15", "VEN 10-12"], ["Aula6"], channel=1)
    schedule.add_lectures("em", ["GIO 8-10"], ["Aula3"], channel=1)
    schedule.add_lectures("didattica", ["MAR 12-13"], ["Aula6"], channel=1)
    schedule.add_lectures("em", ["LUN 8-10", "MAR 15-16", "MER 10-12", "GIO 12-14", "VEN 15-17"], ["Amaldi"], channel=2)
    schedule.add_lectures("didattica", ["VEN 17-18"], ["Amaldi"], channel=1)
    schedule.add_lectures("em", ["LUN 14-16", "MAR 10-12", "MER 14-16", "GIO 9-10", "VEN 10-12"], ["Amaldi"], channel=3)
    schedule.add_lectures("didattica", ["GIO 8-9"], ["Amaldi"], channel=1)

    schedule.add_lectures("labem", ["MER 16-18", "VEN 12-13"], ["Aula6"], channel=1)
    schedule.add_lectures("labem", ["LUN 12-13", "MAR 13-15"], ["Amaldi"], channel=2)
    schedule.add_lectures("labem", ["MAR 12-13"], ["Amaldi"], channel=3)
    schedule.add_lectures("labem", ["GIO 12-14"], ["Aula6"], channel=3)

    schedule.add_lectures("labem", ["LUN 15-19"], ["LabEm"], channel=1)
    schedule.add_lectures("labem", ["MER 15-19"], ["LabEm"], channel=2)
    schedule.add_lectures("labem", ["MAR 15-19"], ["LabEm"], channel=3)
    schedule.add_lectures("rec_labem", ["GIO 15-19"], ["LabEm"])

    # T2.A
    schedule.add_lectures("metodi.astro", ["LUN 13-14", "MAR 8-10", "GIO 10-12", "VEN 8-10"], ["Conversi"])

    schedule.add_lectures("astronomia", ["MER 12-14"], ["Amaldi"])
    schedule.add_lectures("astronomia", ["GIO 14-16"], ["Aula3"])

    schedule.add_lectures("eng", ["GIO 16-20"], ["Aula6"])

    # T3
    schedule.add_lectures("ottica", ["LUN 10-12", "MAR 16-18", "MER 13-14"], ["Aula3"], channel=1)
    schedule.add_lectures("ottica", ["LUN 16-18", "GIO 8-10", "VEN 13-14"], ["Aula6"], channel=2)
    schedule.add_lectures("ottica", ["LUN 16-18"], ["Aula3"], channel=3)
    schedule.add_lectures("ottica", ["GIO 8-9", "VEN 13-15"], ["Aula7"], channel=3)

    schedule.add_lectures("struttura", ["MER 10-12", "VEN 13-16"], ["Conversi"], channel=1)
    schedule.add_lectures("struttura", ["LUN 14-16", "MER 11-14"], ["Aula6"], channel=2)
    schedule.add_lectures("struttura", ["LUN 14-16"], ["Aula3"], channel=3)
    schedule.add_lectures("struttura", ["MER 10-12", "GIO 9-10"], ["Aula7"], channel=3)

    schedule.add_lectures("nucleare", ["LUN 8-10", "MAR 14-16", "MER 12-13"], ["Aula3"], channel=1)
    schedule.add_lectures("nucleare", ["MER 10-11", "GIO 10-12", "VEN 14-16"], ["Aula6"], channel=2)
    schedule.add_lectures("nucleare", ["MER 12-14", "GIO 10-12", "VEN 15-16"], ["Aula7"], channel=3)

    schedule.add_lectures("ottica", ["LUN 15-19"], ["LabOtt"], channel=1)
    schedule.add_lectures("ottica", ["MAR 15-19", "MER 15-19"], ["LabOtt"], channel=2)
    schedule.add_lectures("ottica", ["GIO 15-19"], ["LabOtt"], channel=3)

    # T3.O
    schedule.add_lectures("metodi_IA", ["MAR 12-14", "MER 8-10"], ["Aula3"])
    schedule.add_lectures("metodi_IA", ["GIO 13-15"], ["LabSS-LabAstro"])

    schedule.add_lectures("applicata", ["LUN 12-14", "VEN 10-13"], ["LabCalc"])

    schedule.add_lectures("atmosfera", ["MER 17-19", "VEN 8-10"], ["Aula3"])

    schedule.add_lectures("genevo", ["MAR 8-10"], ["Aula7"])
    schedule.add_lectures("genevo", ["VEN 16-18"], ["Aula6"])

    schedule.add_lectures("elettronica", ["LUN 12-14", "MAR 10-12"], ["Rasetti"])
    schedule.add_lectures("elettronica", ["MER 16-17"], ["Aula3"])

    # M1
    schedule.add_lectures('spectra', ["MAR 12-15", "GIO 12-14"], ["Rasetti"])

    schedule.add_lectures('cond', ["LUN 12-14"], ["Aula7"])
    schedule.add_lectures('cond', ["VEN 12-14"], ["Aula3"])

    schedule.add_lectures('particles', ["LUN 12-14", "MAR 8-10", "MER 10-12"], ["Aula3"])

    schedule.add_lectures('matphys', ["MER 14-16", "GIO 8-10 1-10"], ["Conversi"])
    schedule.add_lectures('matphys', ["LUN 10-12 8-end"], ["Aula7"])

    schedule.add_lectures('neq', ["MAR 16-18", "LUN 16-19"], ["Careri"])

    schedule.add_lectures('gravity', ["MER 8-10", "GIO 12-14", "VEN 12-14"], ["Careri"])

    schedule.add_lectures('nuclear', ["VEN 12-14", "MER 8-10"], ["Rasetti"])

    schedule.add_lectures('liquids', ["MER 10-12"], ["Rasetti"])
    schedule.add_lectures('liquids', ["VEN 10-12"], ["Conversi"])

    schedule.add_lectures('neural', ["LUN 14-16", "MER 12-14"], ["Rasetti"])
    schedule.add_lectures('symm', ["MER 16-18", "VEN 10-12"], ["Careri"])

    schedule.add_lectures('molbio', ["MAR 10-12", "GIO 12-14"], ["Seminari"])

    schedule.add_lectures('exppart', ["LUN 16-18", "MER 18-19"], ["Rasetti"])
    schedule.add_lectures('exppart', ["VEN 16-18"], ["Careri"])

    schedule.add_lectures('labphys2bio', ["LUN 10-12 1-7", "GIO 16-19 1-7", "VEN 9-10 1-7"], ["Careri"])

    schedule.add_lectures('labphys2mat', ["LUN 10-12 1-6"], ["Conversi"])
    schedule.add_lectures('labphys2mat', ["GIO 16-19 1-6", "VEN 9-10 1-6"], ["Rasetti"])

    schedule.add_lectures('labphys2part', ["GIO 16-19"], ["Aula4"])
    schedule.add_lectures('labphys2part', ["VEN 8-10"], ["Aula7"])

    schedule.add_lectures('astroalte', ["MER 16-18"], ["Rasetti"])
    schedule.add_lectures('astroalte', ["VEN 16-18"], ["Rasetti"])

    schedule.add_lectures('extra', ["LUN 14-16", "MAR 14-16"], ["Careri"])

    schedule.add_lectures('astrotheo', ["LUN 10-12", "GIO 10-12"], ["Rasetti"])

    schedule.add_lectures('astropt', ["MAR 12-14", "MER 10-12"], ["Careri"])

    schedule.add_lectures('stardyn', ["LUN 15-17", "MAR 15-17"], ["Fiore"])

    schedule.add_lectures('physcosmo', ["MAR 17-19"], ["Aula6"])
    schedule.add_lectures('physcosmo', ["GIO 8-10"], ["Careri"])
    schedule.add_lectures('physcosmo', ["VEN 9-10"], ["LabCalc"])

    schedule.add_lectures('planets', ["LUN 16-18", "MAR 10-12"], ["LabCalc"])

    schedule.add_lectures('eng2', ["LUN 8-10"], ["Aula7"], check_teacher_overlap=False)
    schedule.add_lectures('eng2', ["MER 8-10"], ["Aula6"], check_teacher_overlap=False)

    schedule.add_lectures('detnacc', ["MAR 10-12", "GIO 14-16"], ["Conversi"])

    schedule.add_lectures('compuarch', ["LUN 14-16"], ["Aula7"])
    schedule.add_lectures('compuarch', ["GIO 12-14"], ["Aula3"])

    schedule.add_lectures('bioteo', ["MAR 8-10", "VEN 14-16"], ["Careri"])

    schedule.add_lectures('phot', ["MAR 16-18", "GIO 14-16"], ["Rasetti"])

    schedule.add_lectures('nonlinear', ["MAR 10-12", "LUN 8-10"], ["Careri"])

    schedule.add_lectures('biophys', ["MER 14-16", "GIO 10-12"], ["Careri"])

    schedule.add_lectures('advanced_ML', ["LUN 8-10"], ["Conversi"])
    schedule.add_lectures('advanced_ML', ["MAR 15-17"], ["Aula7"])
    schedule.add_lectures('advanced_ML', ["GIO 9-12"], ["LabSS-LabAstro"])

    schedule.add_lectures('fundamental', ["MAR 12-14"], ["Conversi"])
    schedule.add_lectures('fundamental', ["VEN 14-16"], ["Aula3"])

    # M2
    schedule.add_lectures('autograv', ["LUN 8-10", "VEN 8-10"], ["Fiore"])

    # Altri impegni didattici
    #schedule.add_lectures("didattica", ["MER 14-16"], ["Aula3"], channel=1)  # Informatica
    #schedule.add_lectures("didattica", ["VEN 10-12"], ["Aula3"], channel=1)  # Matematica
    #schedule.add_lectures("didattica", ["GIO 10-12"], ["Aula8"], channel=1)  # Matematica
    schedule.add_lectures("didattica", ["MAR 14-16"], ["Aula6"], channel=1)  # Genetica
    schedule.add_lectures("didattica", ["GIO 14-16"], ["Aula6"], channel=1)  # Genetica
    #schedule.add_lectures("didattica", ["MAR 10-12"], ["Rasetti"], channel=1)  # Siani/Falasca Jolly

    # Impegni non didattici
    schedule.add_lectures("seminari", ["LUN 14-19", "MAR 16-19", "MER 16-19", "GIO 16-19", "VEN 16-19"], ["Conversi"])
    schedule.add_lectures("CdD/CAD", ["GIO 14-20"], ["Amaldi"])
    #schedule.add_lectures("insegnanti", ["MAR 15-20", "MER 15-20"], ["LabCalc"])

    schedule.export_html("test_orario.html", masked_rooms=['Aula I (Mat.)'], provisional=True)
    schedule.report("test_report.html")
    schedule.simulation("test_simulazione_corsi.html", group='courses')
    schedule.simulation("test_simulazione_docenti.html", group='teachers')


if __name__ == '__main__':
    sys.exit(main())
